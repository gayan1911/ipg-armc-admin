package com.epic.tle.util;

public class KeyInfor {
	
	private String IK;
	private String IK_KVC;
	private String MK;
	private String MK_KVC;
	private String IK_KSN;
	private String ENCR_IK;
	private String ENCR_MK;
	private boolean PROCESSIG;
        private String KID;

    public String getKID() {
        return KID;
    }

    public void setKID(String KID) {
        this.KID = KID;
    }

        
        
	public String getIK() {
		return IK;
	}
	public void setIK(String iK) {
		IK = iK;
	}
	public String getIK_KVC() {
		return IK_KVC;
	}
	public void setIK_KVC(String iK_KVC) {
		IK_KVC = iK_KVC;
	}
	public String getMK() {
		return MK;
	}
	public void setMK(String mK) {
		MK = mK;
	}
	public String getMK_KVC() {
		return MK_KVC;
	}
	public void setMK_KVC(String mK_KVC) {
		MK_KVC = mK_KVC;
	}
	public String getIK_KSN() {
		return IK_KSN;
	}
	public void setIK_KSN(String iK_KSN) {
		IK_KSN = iK_KSN;
	}
	public String getENCR_IK() {
		return ENCR_IK;
	}
	public void setENCR_IK(String eNCR_IK) {
		ENCR_IK = eNCR_IK;
	}
	public String getENCR_MK() {
		return ENCR_MK;
	}
	public void setENCR_MK(String eNCR_MK) {
		ENCR_MK = eNCR_MK;
	}
	public boolean isPROCESSIG() {
		return PROCESSIG;
	}
	public void setPROCESSIG(boolean pROCESSIG) {
		PROCESSIG = pROCESSIG;
	}
	

}
