/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.servermanager.service;

/**
 *
 * @author chalaka_n
 */
public class SessionConfFactory {
    private ServerConfInf factory;

    public SessionConfFactory() {
        this.factory = new SessionConfigurationService();
    }

    public ServerConfInf getSessionService() {
        return factory;
    }

}
