/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.util.alerts;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.epic.tle.util.constant.Configurations;
import com.sun.mail.util.MailSSLSocketFactory;

/**
 *
 * @author gayan_s
 */
public class CMBAlertSender {

    public static boolean sendMail(
            String senderEmail,
            String reciverEmail,
            String subject,
            String message,
            String URL,
            String username,
            String password,
            int port
    ) throws Exception {
        System.out.println("email sending start..!");
        Transport transport = null;
        boolean rc = false;
        try {

            String messageText = message;

            Properties props = System.getProperties();
            props.put("mail.host", URL);
            props.put("mail.transport.protocol.", "smtp");
            props.put("mail.smtp.auth", "false");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.timeout", Configurations.MAIL_TIMEOUT);
            props.put("mail.smtp.connectiontimeout", Configurations.MAIL_TIMEOUT);

            MailSSLSocketFactory sf = new MailSSLSocketFactory();

            sf.setTrustAllHosts(true);

            props.put("mail.smtp.ssl.socketFactory", sf);

            Session mailSession = Session.getDefaultInstance(props, null);

            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(senderEmail));
            InternetAddress[] address = {new InternetAddress(reciverEmail)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject);

            MimeMultipart multipart = new MimeMultipart("related");

            BodyPart messageBodyPart = new MimeBodyPart();
            String htmlText = messageText;
            messageBodyPart.setContent(htmlText, "text/html");

            multipart.addBodyPart(messageBodyPart);

            msg.setContent(multipart);

            transport = mailSession.getTransport("smtp");

            transport.connect();

            transport.sendMessage(msg, msg.getAllRecipients());

            rc = true;
            System.out.println("email successfully sent..!");
        } catch (Exception e) {
            throw e;
        } finally {
            transport.close();
            transport = null;
        }
        return rc;
    }

}
