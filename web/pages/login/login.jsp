<%-- 
    Document   : login
    Created on : Jul 12, 2019, 9:58:31 AM
    Author     : ridmi_g
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Android Experience Hub</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/androidLogin/style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <script type="text/javascript">
            function preventBack() {
                window.history.forward();
            }
            setTimeout("preventBack()", 0);
            window.onunload = function () {
                null
            };

            function clearStorage() {
                localStorage.clear();
            }
        </script>
        <style>
            .errorMessage{
                font-size: 14px;
                font-weight: bold;
            }
            .actionMessage{
                color: green;
            }
        </style>
    </head>
    <body>
        <div class="login-bg">
            <div class="intro-wrapper">
                <div class="intro">
                    <p class="top-section"><span class="quotes">“</span>Single solution
                    </p>
                    <div class="bottom-section">
                        <div></div>
                        <p>to manage every aspect <span class="highlight">Android POS</span><br>from <span
                                class="highlight">bank</span> to <span class="highlight">merchant</span><br>towards <span
                                class="highlight">secure</span> transactions<span class="quotes">”</span></p>
                    </div>
                </div>
            </div>
            <div class="login-white-fg">
                <div class="white-square">
                    <s:form id="login-form" name="login-form" action="loginUser" method="post" >
                        <img class="logo" src="${pageContext.request.contextPath}/androidLogin/image/logo_login_normal.svg" width="60" height="auto" alt="logo">
                        <div class="form-row">
                            <div class="input-wrapper">
                                <img src="${pageContext.request.contextPath}/androidLogin/image/icons/user.svg">
                                <span></span>
                                <input type="text" name="userName" value="" id="userName" class="input" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="input-wrapper">
                                <img src="${pageContext.request.contextPath}/androidLogin/image/icons/lock.svg">
                                <span></span>
                                <input type="password" name="password" id="password" class="input" placeholder="Password"></div>
                        </div>
                        <div class="form-row">
                            <div class="button-wrapper">
                                <button class="button" type="submit" onclick="clearStorage();">Login<img
                                        src="${pageContext.request.contextPath}/androidLogin/image/icons/login_button_arrow.svg"></button>
                            </div>
                        </div>
                        <div class="form-row message-panel">
                            <div id="login-form_" class="ui-widget actionError">
                                <div class="ui-state-error ui-corner-all" style="padding: 0.3em 0.7em; margin-top: 20px;">
                                    <!--                                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: 0.3em;"></span>
                                                                        <span>User name or password cannot be empty</span></p>-->


                                    <div class="msg-panel error-login-msg">
                                        <s:actionerror/>
                                        <s:actionmessage/>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <s:token />
                    </s:form>
                    <p class="footer-description">
                        Android Experience Hub v 1.0 Powered by Epic Lanka Technologies (Pvt.) Ltd.
                    </p>
                </div>
                <div class="white-trangle"></div>
            </div>
        </div>

        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/login_manager.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/submitForm.js"></script>
    </body>
</html>
