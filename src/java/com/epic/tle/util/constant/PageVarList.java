/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.util.constant;


public class PageVarList {
    public static final String REGISTER_USER                =   "0101";
    public static final String USER_VERIFICATION            =   "0102";
    public static final String CHANGE_PASSWORD              =   "0103";
    public static final String PASSWORD_POLICY              =   "0104";
    public static final String REGISTER_USER_PROFILES       =   "0201";
    public static final String REGISTER_FIELD_ENGINEER      =   "0301";
    public static final String TERMINAL_KEY_INJECTION       =   "0302";
    public static final String MPOS_KEY_UTILITY             =   "0303";
    public static final String REGISTER_TERMINAL            =   "0401";
    public static final String NON_FUNCTION_TERMINAL        =   "0402";
    public static final String CHANNEL_MANAGEMENT           =   "0501";
    public static final String LISTENER_MANAGEMENT          =   "0502";
    public static final String NII_CONFIGURATION            =   "0503";
    public static final String SESSION_CONFIGURATIONS       =   "0601";
    public static final String SERVER_STATUS                =   "0602";
    public static final String RESPONSE_CONFIGURATIONS      =   "0603";
    public static final String SERVER_CONFIGURATIONS        =   "0604";
    public static final String PORT_CONFIGURATIONS          =   "0605";
    public static final String SMS_EMAIL_CONFIGURATIONS     =   "0606";
    public static final String LOAD_BALANCE_CONFIGURATIONS  =   "0607";
    public static final String HSM_CONFIGURATIONS           =   "0608";
    public static final String TRANSACTIONS_FAILINGS        =   "0701";
    public static final String SYSTEM_STATUS                =   "0702";
    public static final String SYSTEM_ALERTS                =   "0703";
    public static final String SYSTEM_ALERT_MONITOR         =   "0704";
    public static final String SYSTEM_HISTORY               =   "0705";
    public static final String TRANSACTION_TIME             =   "0706";
    public static final String LOG_FILE_MANAGEMENT          =   "0801";
    public static final String BLOCK_BIN_MANAGEMENT         =   "0901";
    public static final String SYSTEM_BLOCK_BIN             =   "0902";
    public static final String BLOCK_BIN_PROFILE            =   "0903";
    public static final String TERMINAL_REF_PROFILE         =   "0905";
    public static final String LOCAL_BIN_PROFILE            =   "0904";
    public static final String OPERATION_MANAGEMENT         =   "1001";
    public static final String SMS_PROFILE                  =   "0606";
    


    
}
