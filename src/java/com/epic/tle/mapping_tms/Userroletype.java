package com.epic.tle.mapping_tms;
// Generated Jul 30, 2019 11:58:51 AM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Userroletype generated by hbm2java
 */
@Entity
@Table(name="userroletype"
)
public class Userroletype  implements java.io.Serializable {


     private String userroletypecode;
     private String description;
     private Set<Userrole> userroles = new HashSet<Userrole>(0);

    public Userroletype() {
    }

	
    public Userroletype(String userroletypecode) {
        this.userroletypecode = userroletypecode;
    }
    public Userroletype(String userroletypecode, String description, Set<Userrole> userroles) {
       this.userroletypecode = userroletypecode;
       this.description = description;
       this.userroles = userroles;
    }
   
     @Id 

    
    @Column(name="USERROLETYPECODE", unique=true, nullable=false, length=16)
    public String getUserroletypecode() {
        return this.userroletypecode;
    }
    
    public void setUserroletypecode(String userroletypecode) {
        this.userroletypecode = userroletypecode;
    }

    
    @Column(name="DESCRIPTION", length=64)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="userroletype")
    public Set<Userrole> getUserroles() {
        return this.userroles;
    }
    
    public void setUserroles(Set<Userrole> userroles) {
        this.userroles = userroles;
    }




}


