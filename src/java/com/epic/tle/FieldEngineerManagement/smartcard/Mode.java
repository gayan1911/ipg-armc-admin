package com.epic.tle.FieldEngineerManagement.smartcard;

public class Mode {
	public static final String DEV_SMART 				= "01";
	public static final String DEV_KEYINJECTIONTERMINAL             = "02";
	public static final String DEV_MANUAL 				= "03";
        
	public static final String REG_ONLINE 				= "01";;
	public static final String REG_OFFLINE 				= "02";
	
	public static final String ALGO_3DES 				= "01";
	public static final String ALG_RSA                              = "02";

}
