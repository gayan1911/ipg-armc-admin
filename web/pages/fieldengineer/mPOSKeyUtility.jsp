<%-- 
    Document   : mPOSKeyUtility
    Created on : May 10, 2019, 9:48:19 AM
    Author     : ridmi_g
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"  %>  
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>

        <s:set id="vview" var="vview"><s:property value="view" default="true"/></s:set>

        <jsp:include page="../../Styles.jsp" />
        
        <style>
            .body section.app-content .content.innerpage .content-section .content-data .d-row .newright-col {
              display: block;
              float: left;
              width: 50%;
              width: auto;
            }
            body section.app-content .content.innerpage .content-section.search-form .d-row .newright-col .txt-input.width-35 {
               width: 175px;
            }
        </style>

        <script>


            function resetForm() {
               
            }
            function getDetails() {
                
                var terminalId = $('#tid').val();
                var token = $("input[name='RequstToken']").val();
                $.ajax({
                    url: '${pageContext.request.contextPath}/generateTID',
                    data: {tid: terminalId, RequstToken: token},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $stoken = data.token;
                        if (data.success) {
//                            $stoken = data.token;
                            $("input[name='RequstToken']").val($stoken);
                            $('#ik').val(data.ik);
                            $('#ik_kvc').val(data.ik_kvc);
                            $('#mackey').val(data.mackey);
                            $('#mac_kvc').val(data.mac_kvc);
                            $('#ik_ksn').val(data.ik_ksn);
                            $('#mtid').val(data.mtid);

                            document.getElementById("divmsg1").innerHTML =
                                    "<div class='msg-panel add-form-msg successmsg' style='display: block; font-size: 0.8em; text-align: center;'>"
                                    + "<span>"
                                    + "<i class='fa fa-check' aria-hidden='true'></i>"
                                    + data.message
                                    + "</span>"
                                    + "</div>";

                        } else {
                            $("input[name='RequstToken']").val($stoken);
                            document.getElementById("divmsg1").innerHTML =
                                    "<div class='msg-panel add-form-msg errormsg' style='display: block; font-size: 0.8em; text-align: center;'>"
                                    + "<span>"
                                    + "<i class='fa fa-times' aria-hidden='true'></i>"
                                    + data.message
                                    + "</span>"
                                    + "</div>";
                        }
                        
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        if(xhr.responseText.includes("csrfError.jsp")){
                            window.location.replace("${pageContext.request.contextPath}/pages/csrfError.jsp");
                         }else{
                             window.location.href = "${pageContext.request.contextPath}/pages/error.jsp";
                         }
                    }

                });
            }


        </script>

    </head>

    <body style="overflow:hidden">


        <section class="app-content">
            <jsp:include page="../../header.jsp" />             

            <div class="content innerpage">
                <!-- Breadcrumb begin -->
                <div class="breadcrumb">
                    <s:property value="Module"/> <i class="fa fa-angle-double-right" aria-hidden="true"></i><s:property value="Section"/><i class="fa fa-angle-double-right" aria-hidden="true"></i><span id="task" class="active">MPOS Key Utility</span>
                </div>
                <!-- End -->

                <!-- Page title begin -->
                <h1 class="page-title"><s:property value="Section"/><a href="#" class="lnk-back hide-element do-nothing"><i class="fa fa-arrow-left" aria-hidden="true"></i> back</a></h1>
                <!-- End -->

                <!-- Data form begin -->
                <div class="content-section data-form" id="genFillForm">
                    <s:form id="genFillForm1" theme="simple" method="post">
                        <div class="content-data">
                            <!-- Error and success message panel begin -->
                            <div class="msg-panel add-form-msg">
                                <label>&nbsp;</label><div><i class="fa fa-times" aria-hidden="true"></i> <span id="divmsg"></span></div>
                            </div>
                            <!-- End -->
                            <s:div id="divmsg1">

                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <!-- Two colum form row begin -->
                            <div class="d-row">
                                <label class="left-col form-label">TID<sup class="required">*</sup></label>
                                <div class="right-col form-field">
                                    <s:textfield name="tid" id="tid" maxLength="8" cssClass="txt-input width-35"/>
                                </div>
                            </div>
                            <!-- End -->

                            <div class="d-row cpanel">
                                <label class="left-col">&nbsp;</label>
                                <div class="right-col">

                                    <sj:a
                                        button="true" 
                                        onclick="getDetails()" 
                                        cssClass="btn default-button" ><i class="fa fa-floppy-o" aria-hidden="true"></i> GENERATE</sj:a>
                                    </div>
                                </div>

                            </div>
                            <input type="hidden" name="RequstToken" class="RequstToken" value='<%=session.getAttribute("SessionToken")%>'/>
                    </s:form>
                </div>
                <div class="content-section data-form" id="keydetailsform">
                    <s:form id="keydetailsform" theme="simple" method="POST" action="downloadDetails">
                        <div class="content-data">


                            <!-- Two colum form row begin -->
                            <div class="d-row">
                                <label class="left-col form-label">KEY DETAILS</label>

                            </div>
                            <div class="d-row">
                                <label class="left-col form-label"></label>
                            </div>
                            <div class="d-row">
                                <label class="left-col form-label"></label>
                            </div>
                            <div class="d-row">
                                <label class="left-col form-label"></label>
                            </div>

                            <div class="d-row">
                                <label class="col-1 form-label" style="width: 220px;">IK</label>
                                <div class="right-col form-field">
                                    <s:hidden id="mtid" name="mtid"/>
                                    <s:textfield name="ik" id="ik" maxLength="45" cssClass="txt-input width-35" readonly="true"/>
                                    <label class="col-1 form-label" style="width: 23px;">KVC</label>
                                    <s:textfield name="ik_kvc" id="ik_kvc" maxLength="45" cssClass="txt-input width-10" readonly="true"/>
                                </div>                           
<!--                                <label class="col-3 form-label">KVC</label>
                                <div class="col-4 form-field">
                                    <s:textfield name="ik_kvc" id="ik_kvc" maxLength="45" cssClass="txt-input width-35" readonly="true"/>
                                </div>-->
                            </div>
                            <div class="d-row">
                                <label class="col-1 form-label" style="width: 220px;">MAC KEY</label>
                                <div class="right-col form-field">
                                    <s:textfield name="mackey" id="mackey" cssClass="txt-input width-35" maxLength="45" readonly="true"/>
                                    <label class="col-1 form-label" style="width: 23px;">KVC</label>
                                    <s:textfield name="mac_kvc" id="mac_kvc" maxLength="45" cssClass="txt-input width-10" readonly="true"/>
                                </div>                            
<!--                                <label class="col-3 form-label">KVC</label>
                                <div class="col-4 form-field">
                                    <s:textfield name="mac_kvc" id="mac_kvc" cssClass="txt-input width-35" maxLength="45" readonly="true"/>
                                </div>-->
                            </div>
                            <div class="d-row">
                                <label class="col-1 form-label" style="width: 220px;">IK_KSN</label>
                                <div class="right-col form-field">
                                    <s:textfield name="ik_ksn" id="ik_ksn" cssClass="txt-input width-35" maxLength="45" readonly="true"/>
                                </div>                            

                            </div>

                            <div class="d-row cpanel">
                                <label class="left-col">&nbsp;</label>
                                <div class="right-col">
                                    <div class="btn-wrap lnk-match"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                        <s:submit id="downloadbtn" name="downloadbtn" value="Export" cssClass="btn default-button" style="width: 110px;"/></div>

                                </div>
                            </div>
                        </div>

                        <!-- End -->

                    </s:form></div>
            </div>

            <!--End of Body Content-->

            <jsp:include page="../../footer.jsp" />

        </section>

        <script type="text/javascript">
            $(document).ready(function () {
                //Back button event
                $('.lnk-back').on('click', function () {
                    backToMain();
                    $('#task').empty();
                    $('.lnk-back').addClass('hide-element');
                    var text = 'Search Alert Group Profile';
                    $('#task').append(text);
                    return false;
                });

                $(document).ready(function () {

                    setTimeout(function () {
                        $(window).trigger('resize');
                    }, 500);

                });

            });
        </script>

    </body>


</html>
