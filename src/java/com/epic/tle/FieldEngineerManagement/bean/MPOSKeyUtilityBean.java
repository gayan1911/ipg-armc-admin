/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.FieldEngineerManagement.bean;

import java.util.List;
import java.util.Map;

/**
 *
 * @author ridmi_g
 */
public class MPOSKeyUtilityBean {
    
    private String tid;
    private String ik;
    private String ik_kvc;
    private String mackey;
    private String mac_kvc;
    private String ik_ksn;
    
    private String filename;
    //jesper report
    Map parameterMap = null;
    List reportdatalist = null;
    
    private String mtid;

    public String getMtid() {
        return mtid;
    }

    public void setMtid(String mtid) {
        this.mtid = mtid;
    }
 

    private String delsuccess;
    private String message;
    private boolean success;



    private String PagePath = "";
    private String PageCode = "";

    //***************Working Path*************
    private String Module = "";
    private String Section = "";
    
    //***************Token************************
    private String Token;
    
    private boolean view;

    public boolean isView() {
        return view;
    }

    public void setView(boolean view) {
        this.view = view;
    }

    
    public Map getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(Map parameterMap) {
        this.parameterMap = parameterMap;
    }

    public List getReportdatalist() {
        return reportdatalist;
    }

    public void setReportdatalist(List reportdatalist) {
        this.reportdatalist = reportdatalist;
    }

    
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    
    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getIk() {
        return ik;
    }

    public void setIk(String ik) {
        this.ik = ik;
    }

    public String getIk_kvc() {
        return ik_kvc;
    }

    public void setIk_kvc(String ik_kvc) {
        this.ik_kvc = ik_kvc;
    }

    public String getMackey() {
        return mackey;
    }

    public void setMackey(String mackey) {
        this.mackey = mackey;
    }

    public String getMac_kvc() {
        return mac_kvc;
    }

    public void setMac_kvc(String mac_kvc) {
        this.mac_kvc = mac_kvc;
    }

    public String getIk_ksn() {
        return ik_ksn;
    }

    public void setIk_ksn(String ik_ksn) {
        this.ik_ksn = ik_ksn;
    }

    public String getDelsuccess() {
        return delsuccess;
    }

    public void setDelsuccess(String delsuccess) {
        this.delsuccess = delsuccess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

  
    public String getPagePath() {
        return PagePath;
    }

    public void setPagePath(String PagePath) {
        this.PagePath = PagePath;
    }

    public String getPageCode() {
        return PageCode;
    }

    public void setPageCode(String PageCode) {
        this.PageCode = PageCode;
    }

    public String getModule() {
        return Module;
    }

    public void setModule(String Module) {
        this.Module = Module;
    }

    public String getSection() {
        return Section;
    }

    public void setSection(String Section) {
        this.Section = Section;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

 
    
}
