package com.epic.tle.util;

import com.epic.tle.FieldEngineerManagement.smartcard.HSMConnector;
import java.security.Provider;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.jpos.iso.ISOUtil;






public class IKGenerate {
	
	private static byte CONT_A[]  	    =  ISOUtil.hex2byte("C0C0C0C000000000C0C0C0C000000000");
	private static byte CONT_KEY_A[]  	=  ISOUtil.hex2byte("0000000000FF00000000000000FF0000");
	private static byte CONT_KEY_B[]  	=  ISOUtil.hex2byte("000000FF00000000000000FF00000000");
	

	
	public static final String DUKPT_BDK_INDEX_0		= "FFF000";
	public static final String DUKPT_BDK_INDEX_1		= "FFF001";
	public static final String DUKPT_BDK_INDEX_2		= "FFF002";
	public static final String DUKPT_BDK_INDEX_3		= "FFF003";
	public static final String DUKPT_BDK_INDEX_4		= "FFF004";
	public static final String DUKPT_BDK_INDEX_5		= "FFF005";
	public static final String DUKPT_BDK_INDEX_6		= "FFF006";
	public static final String DUKPT_BDK_INDEX_7		= "FFF007";
	public static final String DUKPT_BDK_INDEX_8		= "FFF008";
	public static final String DUKPT_BDK_INDEX_9		= "FFF009";
	public static byte[] KVC_DATA = new byte[16];

	
	
	
private static byte[] encryptIK(SecretKey key, byte[] clearBytes, Provider p) throws Exception {

		
                 
    	Cipher cipher = Cipher.getInstance("DESede/ECB/NoPadding", p.getName());
    	cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(clearBytes);
		
	
	}

public static byte[] decryptIK(SecretKey key, byte[] clearBytes, Provider p) throws Exception {

		
                 
    	Cipher cipher = Cipher.getInstance("DESede/ECB/NoPadding", p.getName());
    	cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(clearBytes);
		
	
	}
public  static  SecretKey getDUKPTIK(byte key[],Provider p)throws Exception{
		
	
	return new SecretKeySpec(key,"DESede");
		
}
	
public static KeyInfor getIK(Provider p,String TID)throws Exception{
		
		
		
		if (TID.length() < 8){TID = ISOUtil.zeropad(TID, 8);}
		TID = TID+"E0000000";
		
		
		
		byte L8_KSN[] 	= ISOUtil.hex2byte(TID);
             
              


		byte LKI[] 		= encryptIK(HSMConnector.getBDK0_IK(), L8_KSN, p);
			
		byte BDK_XOR[]  = null;

                
             
		
		BDK_XOR 	    = ISOUtil.xor(HSMConnector.getBDK0_IK().getEncoded(), CONT_A);
		
		
                
		byte RKI[] 		= encryptIK(new SecretKeySpec(BDK_XOR, "DESede"), L8_KSN, p);
			
		byte IK[] 		= ISOUtil.concat(LKI, RKI);
		
		byte IK_KVC[]      = new byte[3];
		byte MK_KVC[]      = new byte[3];
		
		
		byte encr_data[] = encryptIK (getDUKPTIK(IK, p), KVC_DATA, p);
		
		System.arraycopy(encr_data, 0, IK_KVC, 0, 3);
		
		byte MK_KEY[]    = ISOUtil.xor(IK, encr_data);
		
		encr_data = encryptIK (getDUKPTIK(MK_KEY, p), KVC_DATA, p);
		
		System.arraycopy(encr_data, 0, MK_KVC, 0, 3);
		
		
		byte DB_IK[] 		= encryptIK(HSMConnector.getBDK0_IK(), IK, p);
		byte DB_MK[] 		= encryptIK(HSMConnector.getBDK0_IK(), MK_KEY, p);
		
		KeyInfor kf = new KeyInfor();
		
		kf.setENCR_IK(ISOUtil.hexString(DB_IK));
		kf.setENCR_MK(ISOUtil.hexString(DB_MK));
		kf.setIK(ISOUtil.hexString(IK));
		kf.setMK(ISOUtil.hexString(MK_KEY));
		kf.setIK_KVC(ISOUtil.hexString(IK_KVC));
		kf.setMK_KVC(ISOUtil.hexString(MK_KVC));
		kf.setIK_KSN(TID);
                kf.setKID(ISOUtil.zeropadRight(DUKPT_BDK_INDEX_0, 12));
		kf.setPROCESSIG(true);
			
		return kf;
			
		
		
	}


}
