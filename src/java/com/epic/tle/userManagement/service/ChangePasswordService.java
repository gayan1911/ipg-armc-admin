/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.userManagement.service;

import com.epic.tle.login.bean.SessionUserBean;
import com.epic.tle.mapping.EpicTleUser;
import com.epic.tle.mapping_mechant.Status;
import com.epic.tle.mapping_mechant.Users;
import com.epic.tle.userManagement.bean.ChangePasswordBean;
import com.epic.tle.util.HibernateInit;
import com.epic.tle.util.HibernateInit_Mer;
import com.epic.tle.util.HibernateInit_Tms;
import com.epic.tle.util.Util;
import com.epic.tle.util.constant.Configurations;
import java.util.Date;
import java.util.List;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author danushka_r
 */
public class ChangePasswordService implements ChangePasswordServiceInf{

    SessionUserBean sub = (SessionUserBean) ServletActionContext.getRequest().getSession(false).getAttribute("SessionObject");

    @Override
    public boolean validateOldPassword(ChangePasswordBean changePasswordBean) throws Exception {

        boolean isValidate = false;

        Session session = null;
        Query query;
        List<EpicTleUser> tleWebUser;
        try {
            String hashPasswordValue = Util.generateHash(changePasswordBean.getOldPassword(),Util.getUserRandVal(sub.getUserName()));
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            String sql = "from EpicTleUser wu where wu.username =:username";
            query = session.createQuery(sql);
            query.setString("username", sub.getUserName());
            tleWebUser = query.list();
            if (tleWebUser.size() > 0) {
                isValidate = tleWebUser.get(0).getPassword().equals(hashPasswordValue);
                session.getTransaction().commit();
            }
        } catch (Exception e) {
            if(session!=null){
            session.getTransaction().rollback();
            session.close();
            session = null;
            }
            throw e;
        } finally {            
           if(session!=null){
            session.close();
            session = null;
            }         
        }
        return isValidate;
    }

    @Override
    public boolean updatePassword(ChangePasswordBean changePasswordBean) throws Exception {
        boolean isUpdated = false;
        Session session = null;
        Query query;
        List<EpicTleUser> tleWebUser;
        
        Session session_mer = null;
        Query query_mer;
        List<Users> userses_mer;
        boolean isUpdated_mer = false;
        
        Session session_tms = null;
        Query query_tms;
        List<com.epic.tle.mapping_tms.Users> userses_tms;
        boolean isUpdated_tms = false;
        
        boolean isUp = false;
        String pass="";
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            String sql = "from EpicTleUser wu where wu.username =:username";
            query = session.createQuery(sql);
            query.setString("username", sub.getUserName());
            tleWebUser = query.list();          
            if (tleWebUser.size() > 0) {
                tleWebUser.get(0).setPassword(Util.generateHash(changePasswordBean.getNewPassword(),Util.dataEncrypter(0, tleWebUser.get(0).getRandVal())));
                tleWebUser.get(0).setLastPasswordChangeDate(new Date());
                
                pass=tleWebUser.get(0).getPassword();
                
                session.save(tleWebUser.get(0));
                isUpdated = true;
            }
            
            
            if(Configurations.DB_M_STATUS == 1){
                session_mer = HibernateInit_Mer.sessionFactory_mer.openSession();
                session_mer.beginTransaction();
                String sql_mer = "from Users wu where wu.username =:username";
                query_mer = session_mer.createQuery(sql_mer);
                query_mer.setString("username", sub.getUserName());
                userses_mer = query_mer.list();
                if(userses_mer.size() > 0){
                    userses_mer.get(0).setPassword(pass);
                    Status s=new Status();
                    s.setStatuscode("CHA");
                    userses_mer.get(0).setStatusByPasswordstatus(s);
                    session_mer.save(userses_mer.get(0));
                    isUpdated_mer = true;
                }
            }
            if(Configurations.DB_TMS_STATUS == 1){
                session_tms = HibernateInit_Tms.sessionFactory_tms.openSession();
                session_tms.beginTransaction();
                String sql_tms = "from Users wu where wu.username =:username";
                query_tms = session_tms.createQuery(sql_tms);
                query_tms.setString("username", sub.getUserName());
                userses_tms = query_tms.list();
                if(userses_tms.size() > 0){
                    userses_tms.get(0).setPassword(pass);
                    com.epic.tle.mapping_tms.Status s=new com.epic.tle.mapping_tms.Status();
                    s.setStatuscode("CHA");
                    userses_tms.get(0).setStatusByPasswordstatus(s);
                    session_tms.save(userses_tms.get(0));
                    isUpdated_tms = true;
                }
            }
            
            if(Configurations.DB_M_STATUS == 1 && Configurations.DB_TMS_STATUS == 1){
                if(isUpdated && isUpdated_mer && isUpdated_tms){
                    isUp = true;
                }
               
            }else if(Configurations.DB_M_STATUS == 1){
                if(isUpdated && isUpdated_mer){
                    isUp = true;
                }
            }else if(Configurations.DB_TMS_STATUS == 1){
                if(isUpdated && isUpdated_tms){
                    isUp = true;
                }
            }else{
                isUp=isUpdated;
            }
           
            if(isUp){
                if (session_mer != null) {
                    session_mer.getTransaction().commit();
                }
                if (session_tms != null) {
                    session_tms.getTransaction().commit();
                }
                if (session != null) {
                    session.getTransaction().commit();
                }
            }else{
                if (session_mer != null) {
                    session_mer.getTransaction().rollback();
                }
                if (session_tms != null) {
                    session_tms.getTransaction().rollback();
                }
                if (session != null) {
                    session.getTransaction().rollback();
                }
            }
            
            
        } catch (Exception e) {
            if(session_mer!=null){
            session_mer.getTransaction().rollback();
            session_mer.close();
            session_mer = null;
            }
            if(session_tms!=null){
            session_tms.getTransaction().rollback();
            session_tms.close();
            session_tms = null;
            }
            if(session!=null){
            session.getTransaction().rollback();
            session.close();
            session = null;
            }
            throw e;
        } finally {
            if(session_mer!=null){
            session_mer.flush();
            session_mer.close();
            session_mer = null;
            }
            if(session_tms!=null){
            session_tms.flush();
            session_tms.close();
            session_tms = null;
            }
            if(session!=null){
            session.flush();
            session.close();
            session = null;
            }
        }
        return isUp;
    }
    @Override
    public boolean updatelaselogindate(String userName) throws Exception {
        boolean isUpdated = false;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            Transaction beginTransaction = session.beginTransaction();
            EpicTleUser user = (EpicTleUser) session.createCriteria(EpicTleUser.class, "user")
                    .add(Restrictions.eq("user.username", userName)).uniqueResult();
            user.setLastLoginDate(new Date());
            session.update(user);
            beginTransaction.commit();
            isUpdated = true;
        } catch (Exception e) {
            e.printStackTrace();
            if (session != null) {
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
                session = null;
            }
        }
        return isUpdated;
    }
    @Override
    public Date getLastPasswordUpdateDate(String userName) throws Exception {
        Date last_pass_update_date = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            Transaction beginTransaction = session.beginTransaction();
            EpicTleUser user = (EpicTleUser) session.createCriteria(EpicTleUser.class, "user")
                    .add(Restrictions.eq("user.username", userName)).uniqueResult();
           
            last_pass_update_date = user.getLastPasswordChangeDate();
            beginTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (session != null) {
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
                session = null;
            }
        }
        return last_pass_update_date;
    }
    
}
