/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.userManagement.service;

import com.epic.tle.login.bean.SessionUserBean;
import com.epic.tle.mapping.EpicTleCardholders;
import com.epic.tle.mapping.EpicTleSection;
import com.epic.tle.mapping.EpicTleStatus;
import com.epic.tle.mapping.EpicTleUser;
import com.epic.tle.mapping.EpicTleUserProfile;
import com.epic.tle.mapping_mechant.Status;
import com.epic.tle.mapping_mechant.Userrole;
import com.epic.tle.mapping_mechant.Users;
import com.epic.tle.userManagement.bean.RegisterUserBean;
import com.epic.tle.util.HibernateInit;
import com.epic.tle.util.HibernateInit_Mer;
import com.epic.tle.util.HibernateInit_Tms;
import com.epic.tle.util.Util;
import com.epic.tle.util.constant.Configurations;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author chalaka_n
 */
public class RegisterUserService implements RegisterUserServiceInf {

    SessionUserBean sub = (SessionUserBean) ServletActionContext.getRequest().getSession(false).getAttribute("SessionObject");

    public Map<Integer, String> getUserTypes() throws Exception {
        Map<Integer, String> usertypesmap = new HashMap<Integer, String>();

        List<EpicTleUserProfile> tleUserTypes = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from EpicTleUserProfile wp where wp.epicTleStatus.code=:code";
            Query query = session.createQuery(sql);
            query.setInteger("code", 1);
            tleUserTypes = query.list();

            for (int i = 0; i < tleUserTypes.size(); i++) {
                usertypesmap.put(tleUserTypes.get(i).getCode(), tleUserTypes.get(i).getDescription());
            }
        } catch (Exception e) {
            if (session != null) {
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
                session = null;
            }
        }
        return usertypesmap;
    }

    public Map<Integer, String> getStatusTypes() throws Exception {

        Map<Integer, String> statusTypesMap = new HashMap<Integer, String>();

        List<EpicTleStatus> tleStatusTypes = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from EpicTleStatus wu where wu.code between 1 and 2";
            Query query = session.createQuery(sql);
            tleStatusTypes = query.list();

            for (int i = 0; i < tleStatusTypes.size(); i++) {
                statusTypesMap.put(tleStatusTypes.get(i).getCode(), tleStatusTypes.get(i).getDescription());
            }
        } catch (Exception e) {
            if (session != null) {
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
                session = null;
            }
        }
        return statusTypesMap;

    }

    @Override
    public RegisterUserBean getPagePath(String page, RegisterUserBean inputBean) {
        String module = (page != null && page != "") ? page.substring(0, 2) : "";
        Session session = null;
        String pagePath = "";

        try {

            if (module != null && module != "") {
                session = HibernateInit.sessionFactory.openSession();
                session.beginTransaction();
                EpicTleSection epicTleSection = (EpicTleSection) session.get(EpicTleSection.class, page);
                String mod = epicTleSection.getEpicTleModule().getDescription();
                String sect = epicTleSection.getSectionName();

                inputBean.setModule(mod);
                inputBean.setSection(sect);
            }

        } catch (Exception e) {
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.getTransaction().commit();
                session.close();
                session = null;
            }
        }

        return inputBean;
    }

    @Override
    public boolean checkUserName(String userName) throws Exception {
        boolean isUsername = false;
        List<EpicTleUser> tleWebUser = null;
        Session session = null;
        Query query = null;

        boolean isUsername_mer = false;
        List<Users> userses_mer = null;
        Session session_mer = null;
        Query query_mer = null;
        
        boolean isUsername_tms = false;
        List<com.epic.tle.mapping_tms.Users> userses_tms = null;
        Session session_tms = null;
        Query query_tms = null;
        
        boolean isUse = false;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from EpicTleUser wu where wu.username =:username";
            query = session.createQuery(sql);
            query.setString("username", userName);
            tleWebUser = query.list();
            if (0 < tleWebUser.size()) {
                int statusCode = 0;
                for (int i = 0; i < tleWebUser.size(); i++) {
                    statusCode = tleWebUser.get(i).getEpicTleStatus().getCode();
                    if (statusCode == 1) {
                        isUsername = true;
                    }
                }

            }

            if (Configurations.DB_M_STATUS == 1) {
                session_mer = HibernateInit_Mer.sessionFactory_mer.openSession();
                String sql_mer = "from Users wu where wu.username =:username";
                query_mer = session_mer.createQuery(sql_mer);
                query_mer.setString("username", userName);
                userses_mer = query_mer.list();
                if (0 < userses_mer.size()) {
                    for (int i = 0; i < userses_mer.size(); i++) {
                        isUsername_mer = true;
                    }
                }
            }
            if (Configurations.DB_TMS_STATUS == 1) {
                session_tms = HibernateInit_Tms.sessionFactory_tms.openSession();
                String sql_tms = "from Users wu where wu.username =:username";
                query_tms = session_tms.createQuery(sql_tms);
                query_tms.setString("username", userName);
                userses_tms = query_tms.list();
                if (0 < userses_tms.size()) {
                    for (int i = 0; i < userses_tms.size(); i++) {
                        isUsername_tms = true;
                    }
                }
            }
            
            if (Configurations.DB_M_STATUS == 1 && Configurations.DB_TMS_STATUS == 1) {
                if (isUsername && isUsername_mer && isUsername_tms) {
                    isUse = true;
                }

            } else if (Configurations.DB_M_STATUS == 1) {
                if (isUsername && isUsername_mer) {
                    isUse = true;
                }
            } else if (Configurations.DB_TMS_STATUS == 1) {
                if (isUsername && isUsername_tms) {
                    isUse = true;
                }
            } else {
                isUse = isUsername;
            }

        } catch (Exception e) {
            if (session_mer != null) {
                session_mer.close();
                session_mer = null;
            }
            if (session_tms != null) {
                session_tms.close();
                session_tms = null;
            }
            if (session != null) {
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session_mer != null) {
                session_mer.close();
                session_mer = null;
            }
            if (session_tms != null) {
                session_tms.close();
                session_tms = null;
            }
            if (session != null) {
                session.close();
                session = null;
            }
        }
        return isUse;
    }

    public boolean checkEmail(String email, String userName) throws Exception {
        boolean isEmail = false;
        List<EpicTleUser> tleWebUser = null;
        Session session = null;
        Query query = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from EpicTleUser wu where wu.username!=:username and wu.email =:email";
            query = session.createQuery(sql);
            query.setString("email", email);
            query.setString("username", userName);
            tleWebUser = query.list();
            if (0 < tleWebUser.size()) {
                isEmail = true;
            }
        } catch (Exception e) {
            if (session != null) {
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
                session = null;
            }
        }
        return isEmail;
    }

    @Override
    public boolean addUser(RegisterUserBean inputBean) throws Exception {
        boolean isAddUser = false;
        EpicTleUser tleWebUser = null;
        Session session = null;
        Session session_mer = null;
        Users users = null;
        Session session_tms = null;
        com.epic.tle.mapping_tms.Users users_tms = null;
        try {
            String randomValue = Util.getRandomVal(10);
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            tleWebUser = new EpicTleUser();
            tleWebUser.setName(inputBean.getName());
            tleWebUser.setUsername(inputBean.getUserName());
            tleWebUser.setRandVal(Util.dataEncrypter(1, randomValue));
            tleWebUser.setPassword(Util.generateHash(inputBean.getPassword(), randomValue));
            tleWebUser.setEmail(inputBean.getEmail());
            EpicTleUserProfile tleuser = new EpicTleUserProfile();
            tleuser.setCode(Integer.parseInt(inputBean.getUserType()));
            tleWebUser.setEpicTleUserProfile(tleuser);

            EpicTleStatus tlestatus = (EpicTleStatus) session.get(EpicTleStatus.class, 9);
            tleWebUser.setEpicTleStatus(tlestatus);
            tleWebUser.setCreatedate(Util.getLocalDate());
            tleWebUser.setCreateusername(sub.getUserName());
            tleWebUser.setLastPasswordChangeDate(new Date());
            tleWebUser.setLastRawUpdateDateTime(new Date());
            tleWebUser.setLastPasswordResetDate(new Date());
            tleWebUser.setLastLoginDate(new Date());

            Serializable a = session.save(tleWebUser);

            if (Configurations.DB_M_STATUS == 1) {
                session_mer = HibernateInit_Mer.sessionFactory_mer.openSession();
                session_mer.beginTransaction();
                users = new Users();
                users.setUsername(tleWebUser.getUsername());
                users.setPassword(tleWebUser.getPassword());
                Userrole userrole = new Userrole();
                userrole.setUserrolecode(inputBean.getUserTypeMer());
                users.setUserrole(userrole);
                users.setFullname(tleWebUser.getName());
                users.setCreatedtime(tleWebUser.getCreatedate());
                users.setLastupdateduser(tleWebUser.getCreateusername());
                Status s = new Status();
                s.setStatuscode("ACT");
                users.setStatusByStatus(s);
                Status s1 = new Status();
                s1.setStatuscode("NEW");
                users.setStatusByPasswordstatus(s1);
                users.setAttempts((byte) 0);
                users.setLastupdatedtime(new Date());
               

                session_mer.save(users);

            }
            if (Configurations.DB_TMS_STATUS == 1) {
                session_tms = HibernateInit_Tms.sessionFactory_tms.openSession();
                session_tms.beginTransaction();
                users_tms = new com.epic.tle.mapping_tms.Users();
                users_tms.setUsername(tleWebUser.getUsername());
                users_tms.setPassword(tleWebUser.getPassword());
                com.epic.tle.mapping_tms.Userrole userrole_tms = new com.epic.tle.mapping_tms.Userrole();
                userrole_tms.setId(Integer.parseInt(inputBean.getUserTypeTms()));
                users_tms.setUserrole(userrole_tms);
                users_tms.setFullname(tleWebUser.getName());
                users_tms.setCreatedtime(tleWebUser.getCreatedate());
                users_tms.setLastupdateduser(tleWebUser.getCreateusername());
                com.epic.tle.mapping_tms.Status s_tms = new com.epic.tle.mapping_tms.Status();
                s_tms.setStatuscode("ACT");
                users_tms.setStatusByStatus(s_tms);
                com.epic.tle.mapping_tms.Status s1_tms = new com.epic.tle.mapping_tms.Status();
                s1_tms.setStatuscode("NEW");
                users_tms.setStatusByPasswordstatus(s1_tms);
                users_tms.setAttempts((byte) 0);
                users_tms.setLastupdatedtime(new Date());

                session_tms.save(users_tms);
            }

            int id = a.hashCode();
            Util.updatePasswordHistory(inputBean.getPassword(), sub.getUserName(), id, randomValue);
            isAddUser = true;
        } catch (Exception e) {
            if (session_mer != null) {
                session_mer.getTransaction().rollback();
                session_mer.close();
                session_mer = null;
            }
            if (session_tms != null) {
                session_tms.getTransaction().rollback();
                session_tms.close();
                session_tms = null;
            }
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session_mer != null) {

                session_mer.getTransaction().commit();
                session_mer.flush();
                session_mer.close();
                session_mer = null;
            }
            if (session_tms != null) {

                session_tms.getTransaction().commit();
                session_tms.flush();
                session_tms.close();
                session_tms = null;
            }
            if (session != null) {
                session.getTransaction().commit();
                session.flush();
                session.close();
                session = null;
            }
        }
        return isAddUser;
    }

    //should edit
    public List<RegisterUserBean> loadUsers(RegisterUserBean inputBean, int max, int first, String orderBy) throws Exception {

        List<RegisterUserBean> dataList = new ArrayList<RegisterUserBean>();
        Session session = null;
        Query querySearch = null;
        try {
            if (orderBy.equals("") || orderBy == null) {
                orderBy = " order by wu.createdate desc ";
            }
            long count = 0;

            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            if (inputBean.getSearchstatus() != null && !inputBean.getSearchstatus().equals("-1")) {
                String sqlCount = "select count(username) from EpicTleUser wu where wu.name LIKE :name and wu.epicTleUserProfile.description LIKE :userRole and wu.epicTleStatus.code LIKE :statuscode" + orderBy;
                Query queryCount = session.createQuery(sqlCount);
                queryCount.setParameter("name", "%" + inputBean.getSearchName() + "%");
                queryCount.setParameter("userRole", "%" + inputBean.getUserRole() + "%");
                queryCount.setString("statuscode", "%" + inputBean.getSearchstatus() + "%");
                Iterator itCount = queryCount.iterate();
                count = (Long) itCount.next();
            } else {
                String sqlCount = "select count(username) from EpicTleUser wu where wu.name LIKE :name and wu.epicTleUserProfile.description LIKE :userRole" + orderBy;
                Query queryCount = session.createQuery(sqlCount);
                queryCount.setParameter("name", "%" + inputBean.getSearchName() + "%");
                queryCount.setParameter("userRole", "%" + inputBean.getUserRole() + "%");
                Iterator itCount = queryCount.iterate();
                count = (Long) itCount.next();
            }

            if (count > 0) {

                if (inputBean.getSearchstatus() != null && !inputBean.getSearchstatus().equals("-1")) {
                    String sqlSearch = "from EpicTleUser wu where wu.name LIKE :name and wu.epicTleUserProfile.description LIKE :userRole and wu.epicTleStatus.code LIKE :statuscode" + orderBy;
                     querySearch = session.createQuery(sqlSearch);
                    querySearch.setParameter("name", "%" + inputBean.getSearchName() + "%");
                    querySearch.setParameter("userRole", "%" + inputBean.getUserRole() + "%");
                    querySearch.setString("statuscode", "%" + inputBean.getSearchstatus() + "%");
                    
                } else {
                    String sqlSearch = "from EpicTleUser wu where wu.name LIKE :name and wu.epicTleUserProfile.description LIKE :userRole" + orderBy;
                     querySearch = session.createQuery(sqlSearch);
                    querySearch.setParameter("name", "%" + inputBean.getSearchName() + "%");
                    querySearch.setParameter("userRole", "%" + inputBean.getUserRole() + "%");
                }

                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    RegisterUserBean databean = new RegisterUserBean();
                    EpicTleUser objBean = (EpicTleUser) it.next();
                    try {
                        databean.setName(objBean.getName());
                    } catch (NullPointerException npe) {
                        databean.setName("--");
                    }
                    try {
                        databean.setEmail(objBean.getEmail().isEmpty() ? "--" : objBean.getEmail());
                    } catch (NullPointerException npe) {
                        databean.setEmail("--");
                    }
                    try {
                        databean.setUserName(objBean.getUsername());
                    } catch (NullPointerException npe) {
                        databean.setUserName("--");
                    }
                    try {
                        databean.setUserType(objBean.getEpicTleUserProfile().getDescription());
                    } catch (NullPointerException npe) {
                        databean.setUserType("--");
                    }
                    try {
                        databean.setUserTypeId("" + objBean.getEpicTleUserProfile().getCode());
                    } catch (NullPointerException npe) {
                        databean.setUserType("--");
                    }
                    try {
                        databean.setStatusType("" + objBean.getEpicTleStatus().getCode());
                    } catch (NullPointerException npe) {
                        databean.setUserType("--");
                    }
                    try {
                        databean.setDate(objBean.getCreatedate().toString());
                    } catch (NullPointerException npe) {
                        databean.setUserType("--");
                    }
                    databean.setFullCount(count);

                    dataList.add(databean);
                }

            }
        } catch (Exception e) {
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.getTransaction().commit();
                session.close();
                session = null;
            }
        }
        return dataList;
    }

    public boolean deleteUser(String duserName) throws Exception {
        boolean isUserDeleted = false;
        Session session = null;
        Query query = null;
        boolean isUserDeleted_mer = false;
        Session session_mer = null;
        Query query_mer = null;
        boolean isUserDeleted_tms = false;
        Session session_tms = null;
        Query query_tms = null;
        boolean isde = false;
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            String sql = "delete EpicTleUser wu"
                    + "  where wu.username =:username";
            query = session.createQuery(sql);
            query.setString("username", duserName);
            int result = query.executeUpdate();

            if (1 == result) {
                isUserDeleted = true;
            }

            if (Configurations.DB_M_STATUS == 1) {
                session_mer = HibernateInit_Mer.sessionFactory_mer.openSession();
                session_mer.beginTransaction();
                String sql_mer = "delete Users wu"
                        + "  where wu.username =:username";
                query_mer = session_mer.createQuery(sql_mer);
                query_mer.setString("username", duserName);
                int result_mer = query_mer.executeUpdate();
                if (1 == result_mer) {
                    isUserDeleted_mer = true;
                }
            }
            if (Configurations.DB_TMS_STATUS == 1) {
                session_tms = HibernateInit_Tms.sessionFactory_tms.openSession();
                session_tms.beginTransaction();
                String sql_tms = "delete Users wu"
                        + "  where wu.username =:username";
                query_tms = session_tms.createQuery(sql_tms);
                query_tms.setString("username", duserName);
                int result_tms = query_tms.executeUpdate();
                if (1 == result_tms) {
                    isUserDeleted_tms = true;
                }
            }

            if (Configurations.DB_M_STATUS == 1 && Configurations.DB_TMS_STATUS == 1) {
                if (isUserDeleted && isUserDeleted_mer && isUserDeleted_tms) {
                    isde = true;
                }

            } else if (Configurations.DB_M_STATUS == 1) {
                if (isUserDeleted && isUserDeleted_mer) {
                    isde = true;
                }
            } else if (Configurations.DB_TMS_STATUS == 1) {
                if (isUserDeleted && isUserDeleted_tms) {
                    isde = true;
                }
            } else {
                isde = isUserDeleted;
            }

            if (isde) {
                if (session_mer != null) {
                    session_mer.getTransaction().commit();
                }
                if (session_tms != null) {
                    session_tms.getTransaction().commit();
                }
                if (session != null) {
                    session.getTransaction().commit();
                }
            } else {
                if (session_mer != null) {
                    session_mer.getTransaction().rollback();
                }
                if (session_tms != null) {
                    session_tms.getTransaction().rollback();
                }
                if (session != null) {
                    session.getTransaction().rollback();
                }
            }

        } catch (Exception e) {
            if (session_mer != null) {
                session_mer.getTransaction().rollback();
                session_mer.close();
                session_mer = null;
            }
            if (session_tms != null) {
                session_tms.getTransaction().rollback();
                session_tms.close();
                session_tms = null;
            }
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
                session = null;
            }
            e.printStackTrace();
            throw e;
        } finally {
            if (session_mer != null) {
                session_mer.flush();
                session_mer.close();
                session_mer = null;
            }
            if (session_tms != null) {
                session_tms.flush();
                session_tms.close();
                session_tms = null;
            }
            if (session != null) {
                session.flush();
                session.close();
                session = null;
            }
        }
//        return isUserDeleted;
        return isde;
    }

    @Override
    public void findUser(RegisterUserBean inputBean) throws Exception {
        List<EpicTleUser> finfuserlist = null;
        List<Users> finfuserlist_mer = null;
        List<com.epic.tle.mapping_tms.Users> finfuserlist_tms = null;
        Session session = null;
        Query query = null;
        Session session_mer = null;
        Query query_mer = null;
        Session session_tms = null;
        Query query_tms = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from EpicTleUser wu where wu.username =:username";
            query = session.createQuery(sql);
            query.setString("username", inputBean.getUpuserName());
            finfuserlist = query.list();

            if (0 < finfuserlist.size()) {
                inputBean.setUpName(finfuserlist.get(0).getName());
                inputBean.setUpEmail(finfuserlist.get(0).getEmail());
                inputBean.setUpuserName(finfuserlist.get(0).getUsername());
                inputBean.setUpuserTypeId(finfuserlist.get(0).getEpicTleUserProfile().getCode());
                int code = finfuserlist.get(0).getEpicTleStatus().getCode();
                inputBean.setUpstatus(code);
                boolean ism = true;
                if (code == 1) {
                    ism = false;
                } else if (code == 2) {
                    ism = false;
                }else if (code == 11){ // user expired status
                    ism = false;
                }
                
                if(code == 1 || code == 2){
                    inputBean.setStatusTypeMap(getStatusTypes());
                }else if(code == 11){
                    inputBean.setStatusTypeMap(getStatusTypessearch());
                }
                
                inputBean.setIsNewMember(ism);
            }

            if (Configurations.DB_M_STATUS == 1) {
                session_mer = HibernateInit_Mer.sessionFactory_mer.openSession();
                String sql_mer = "from Users u where u.username =:username";
                query_mer = session_mer.createQuery(sql_mer);
                query_mer.setString("username", inputBean.getUpuserName());
                finfuserlist_mer = query_mer.list();

                if (0 < finfuserlist_mer.size()) {
                    inputBean.setUpuserTypeMer(finfuserlist_mer.get(0).getUserrole().getUserrolecode());
                }
            }
            if (Configurations.DB_TMS_STATUS == 1) {
                session_tms = HibernateInit_Tms.sessionFactory_tms.openSession();
                String sql_tms = "from Users wu where wu.username =:username";
                query_tms = session_tms.createQuery(sql_tms);
                query_tms.setString("username", inputBean.getUpuserName());
                finfuserlist_tms = query_tms.list();

                if (0 < finfuserlist_tms.size()) {
                    inputBean.setUpuserTypeTms(finfuserlist_tms.get(0).getUserrole().getId().toString());
                }
            }
        } catch (Exception e) {
            if (session != null) {
                session.close();
                session = null;
            }
            if (session_mer != null) {
                session_mer.close();
                session_mer = null;
            }
            if (session_tms != null) {
                session_tms.close();
                session_tms = null;
            }
            e.printStackTrace();
            throw e;
        } finally {
            if (session != null) {
                session.close();
                session = null;
            }
            if (session_mer != null) {
                session_mer.close();
                session_mer = null;
            }
            if (session_tms != null) {
                session_tms.close();
                session_tms = null;
            }
        }

    }

    @Override
    public boolean updateUser(RegisterUserBean inputBean) throws Exception {
        boolean isUpdated = false;
        Session session = null;
        Query query = null;
        List<EpicTleUser> tleWebUser = null;
        Query query_mer = null;
        List<Users> user_mer = null;
        Query query_tms = null;
        List<com.epic.tle.mapping_tms.Users> user_tms = null;
        Session session_mer = null;
        Session session_tms = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            String sql = "from EpicTleUser wu where wu.username =:username";
            query = session.createQuery(sql);
            query.setString("username", inputBean.getUpuserName());
            tleWebUser = query.list();
            if (tleWebUser.size() > 0) {
                tleWebUser.get(0).setName(inputBean.getUpName());
                tleWebUser.get(0).setEmail(inputBean.getUpEmail());
                EpicTleUserProfile tu = new EpicTleUserProfile();
                tu.setCode(inputBean.getUpuserTypeId());
                tleWebUser.get(0).setEpicTleUserProfile(tu);
                if (inputBean.getUpstatus() > 0) {
                    EpicTleStatus tlestatus = (EpicTleStatus) session.get(EpicTleStatus.class, inputBean.getUpstatus());
                    
                    int previous_status = tleWebUser.get(0).getEpicTleStatus().getCode();
                    tleWebUser.get(0).setEpicTleStatus(tlestatus);
                    
                    //when status active (status changed), last login date update
                   
                    if(tlestatus.getCode() == com.epic.tle.util.constant.Status.ACTIVE){
                        if(tlestatus.getCode() != previous_status){
                            tleWebUser.get(0).setLastLoginDate(new Date());
                        }
                        
                    }else if(tlestatus.getCode() == com.epic.tle.util.constant.Status.INACTIVE){
                        if(previous_status == com.epic.tle.util.constant.Status.USER_EXPIRED){
                            tleWebUser.get(0).setLastLoginDate(new Date());
                        }
                    }
                }
                session.save(tleWebUser.get(0));

            }

            if (Configurations.DB_M_STATUS == 1) {
                session_mer = HibernateInit_Mer.sessionFactory_mer.openSession();
                session_mer.beginTransaction();
                String sql_mer = "from Users wu where wu.username =:username";
                query_mer = session_mer.createQuery(sql_mer);
                query_mer.setString("username", inputBean.getUpuserName());

                user_mer = query_mer.list();
                if (user_mer.size() > 0) {
                    user_mer.get(0).setFullname(inputBean.getUpName());
                    Userrole tu = new Userrole();
                    tu.setUserrolecode(inputBean.getUpuserTypeMer());
                    user_mer.get(0).setUserrole(tu);

                    session_mer.save(user_mer.get(0));

                }
            }
            if (Configurations.DB_TMS_STATUS == 1) {
                session_tms = HibernateInit_Tms.sessionFactory_tms.openSession();
                session_tms.beginTransaction();
                String sql_tms = "from Users wu where wu.username =:username";
                query_tms = session_tms.createQuery(sql_tms);
                query_tms.setString("username", inputBean.getUpuserName());

                user_tms = query_tms.list();
                if (user_tms.size() > 0) {
                    user_tms.get(0).setFullname(inputBean.getUpName());
                    com.epic.tle.mapping_tms.Userrole tu = new com.epic.tle.mapping_tms.Userrole();
                    tu.setId(Integer.parseInt(inputBean.getUpuserTypeTms()));
                    user_tms.get(0).setUserrole(tu);

                    session_tms.save(user_tms.get(0));

                }
            }

            isUpdated = true;

        } catch (Exception e) {

            if (session_mer != null) {
                session_mer.getTransaction().rollback();
                session_mer.close();
                session_mer = null;
            }
            if (session_tms != null) {
                session_tms.getTransaction().rollback();
                session_tms.close();
                session_tms = null;
            }
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
                session = null;
            }
            throw e;
        } finally {

            if (session_mer != null) {
                session_mer.getTransaction().commit();
                session_mer.flush();
                session_mer.close();
                session_mer = null;
            }
            if (session_tms != null) {
                session_tms.getTransaction().commit();
                session_tms.flush();
                session_tms.close();
                session_tms = null;
            }
            if (session != null) {
                session.getTransaction().commit();
                session.flush();
                session.close();
                session = null;
            }
        }
        return isUpdated;
    }

    @Override
    public boolean updateLastPaswwordResetDate(String userName) throws Exception {
        boolean isUpdated = false;
        Session session = null;
        Query query = null;
        List<EpicTleUser> tleWebUser = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            String sql = "from EpicTleUser wu where wu.username =:username";
            query = session.createQuery(sql);
            query.setString("username", userName);
            tleWebUser = query.list();
            if (tleWebUser.size() > 0) {
                tleWebUser.get(0).setLastPasswordResetDate(new Date());
                session.save(tleWebUser.get(0));
                session.getTransaction().commit();
                isUpdated = true;
            }
        } catch (Exception e) {
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
                session = null;
            }
        }
        return isUpdated;
    }

    @Override
    public boolean updateUserStatus(int status, String username) throws Exception {
        boolean isUpdated = false;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            Transaction beginTransaction = session.beginTransaction();
            EpicTleUser user = (EpicTleUser) session.createCriteria(EpicTleUser.class, "user")
                    .add(Restrictions.eq("user.username", username)).uniqueResult();
            user.setEpicTleStatus(new EpicTleStatus(status));
            session.update(user);
            beginTransaction.commit();
            isUpdated = true;
        } catch (Exception e) {
            e.printStackTrace();
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
                session = null;
            }
        }
        return isUpdated;
    }

    @Override
    public boolean updatePassword(String password, String username) throws Exception {
        boolean isUpdated = false;
        Session session = null;
        EpicTleUser user;
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            user = (EpicTleUser) session
                    .createCriteria(EpicTleUser.class, "user")
                    .add(Restrictions.eq("user.username", username))
                    .uniqueResult();
            user.setPassword(Util.generateHash(password, Util.getUserRandVal(username)));
            session.update(user);
        } catch (Exception e) {
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.getTransaction().commit();
                session.flush();
                session.close();
                session = null;
            }
        }
        return isUpdated;
    }

    @Override
    public boolean isAnyUserCreateCardholders(String duserName) throws Exception {
        boolean ok = false;
        Session session = null;
        try {
            List<EpicTleCardholders> epicTleCardholderses = null;

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from EpicTleCardholders wu where wu.userid.username =:username";
            Query query = session.createQuery(sql);
            query.setString("username", duserName);
            epicTleCardholderses = query.list();

            if (epicTleCardholderses.size() > 0) {
                ok = true;
            }
        } catch (Exception e) {
            if (session != null) {
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
                session = null;
            }
        }

        return ok;
    }

    @Override
    public boolean updateUserStatusMer(String status, String username) throws Exception {
        boolean isUpdated = false;
        Session session_mer = null;
        try {
            session_mer = HibernateInit_Mer.sessionFactory_mer.openSession();
            Transaction beginTransaction_mer = session_mer.beginTransaction();
            Users user = (Users) session_mer.createCriteria(Users.class, "user")
                    .add(Restrictions.eq("user.username", username)).uniqueResult();
            Status s = new Status();
            s.setStatuscode(status);
            user.setStatusByPasswordstatus(s);
            session_mer.update(user);
            beginTransaction_mer.commit();
            isUpdated = true;
        } catch (Exception e) {
            e.printStackTrace();
            if (session_mer != null) {
                session_mer.getTransaction().rollback();
                session_mer.close();
                session_mer = null;
            }
            throw e;
        } finally {
            if (session_mer != null) {
                session_mer.close();
                session_mer = null;
            }
        }
        return isUpdated;
    }

    @Override
    public boolean updateUserStatusTms(String status, String username) throws Exception {
        boolean isUpdated = false;
        Session session_tms = null;
        try {
            session_tms = HibernateInit_Tms.sessionFactory_tms.openSession();
            Transaction beginTransaction_tms = session_tms.beginTransaction();
            com.epic.tle.mapping_tms.Users user = (com.epic.tle.mapping_tms.Users) session_tms.createCriteria(com.epic.tle.mapping_tms.Users.class, "user")
                    .add(Restrictions.eq("user.username", username)).uniqueResult();
            com.epic.tle.mapping_tms.Status s = new com.epic.tle.mapping_tms.Status();
            s.setStatuscode(status);
            user.setStatusByPasswordstatus(s);
            session_tms.update(user);
            beginTransaction_tms.commit();
            isUpdated = true;
        } catch (Exception e) {
            e.printStackTrace();
            if (session_tms != null) {
                session_tms.getTransaction().rollback();
                session_tms.close();
                session_tms = null;
            }
            throw e;
        } finally {
            if (session_tms != null) {
                session_tms.close();
                session_tms = null;
            }
        }
        return isUpdated;
    }
    @Override
    public Map<Integer, String> getStatusTypessearch() throws Exception {

        Map<Integer, String> statusTypesMap = new HashMap<Integer, String>();

        List<EpicTleStatus> tleStatusTypes = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from EpicTleStatus wu where wu.code=1 or wu.code=2 or wu.code=11";
            Query query = session.createQuery(sql);
            tleStatusTypes = query.list();

            for (int i = 0; i < tleStatusTypes.size(); i++) {
                statusTypesMap.put(tleStatusTypes.get(i).getCode(), tleStatusTypes.get(i).getDescription());
            }
        } catch (Exception e) {
            if (session != null) {
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
                session = null;
            }
        }
        return statusTypesMap;

    }
}
