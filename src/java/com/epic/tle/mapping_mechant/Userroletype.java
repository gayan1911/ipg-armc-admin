package com.epic.tle.mapping_mechant;
// Generated Jun 25, 2019 8:22:54 AM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Userroletype generated by hbm2java
 */
@Entity
@Table(name="userroletype"
    
)
public class Userroletype  implements java.io.Serializable {


     private String userroletypecode;
     private Status status;
     private String description;
     private Date lastupdatedtime;
     private String lastupdateduser;
     private Date createdtime;
     private Set<Userrole> userroles = new HashSet<Userrole>(0);

    public Userroletype() {
    }

	
    public Userroletype(String userroletypecode, Status status, Date lastupdatedtime, Date createdtime) {
        this.userroletypecode = userroletypecode;
        this.status = status;
        this.lastupdatedtime = lastupdatedtime;
        this.createdtime = createdtime;
    }
    public Userroletype(String userroletypecode, Status status, String description, Date lastupdatedtime, String lastupdateduser, Date createdtime, Set<Userrole> userroles) {
       this.userroletypecode = userroletypecode;
       this.status = status;
       this.description = description;
       this.lastupdatedtime = lastupdatedtime;
       this.lastupdateduser = lastupdateduser;
       this.createdtime = createdtime;
       this.userroles = userroles;
    }
   
     @Id 

    
    @Column(name="USERROLETYPECODE", unique=true, nullable=false, length=16)
    public String getUserroletypecode() {
        return this.userroletypecode;
    }
    
    public void setUserroletypecode(String userroletypecode) {
        this.userroletypecode = userroletypecode;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="STATUS", nullable=false)
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }

    
    @Column(name="DESCRIPTION", length=64)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDTIME", nullable=false, length=19)
    public Date getLastupdatedtime() {
        return this.lastupdatedtime;
    }
    
    public void setLastupdatedtime(Date lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    
    @Column(name="LASTUPDATEDUSER", length=64)
    public String getLastupdateduser() {
        return this.lastupdateduser;
    }
    
    public void setLastupdateduser(String lastupdateduser) {
        this.lastupdateduser = lastupdateduser;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDTIME", nullable=false, length=19)
    public Date getCreatedtime() {
        return this.createdtime;
    }
    
    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="userroletype")
    public Set<Userrole> getUserroles() {
        return this.userroles;
    }
    
    public void setUserroles(Set<Userrole> userroles) {
        this.userroles = userroles;
    }




}


