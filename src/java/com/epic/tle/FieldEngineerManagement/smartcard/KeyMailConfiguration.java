package com.epic.tle.FieldEngineerManagement.smartcard;

public class KeyMailConfiguration {

    public static int DATA_RATE = 9600;
    public static int DATABITS = 8;
    public static int STOPBITS = 1;
    public static int PARITY = 0;
    public static int STATUS = 1;
    public static String PORT = "COM";
}
