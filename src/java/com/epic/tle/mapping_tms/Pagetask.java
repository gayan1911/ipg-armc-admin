package com.epic.tle.mapping_tms;
// Generated Jul 30, 2019 11:58:51 AM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Pagetask generated by hbm2java
 */
@Entity
@Table(name="pagetask"
)
public class Pagetask  implements java.io.Serializable {


     private Integer id;
     private Page page;
     private Task task;
     private Set<Pagetaskinstance> pagetaskinstances = new HashSet<Pagetaskinstance>(0);
     private Set<Pagetaskinstitute> pagetaskinstitutes = new HashSet<Pagetaskinstitute>(0);

    public Pagetask() {
    }

	
    public Pagetask(Page page, Task task) {
        this.page = page;
        this.task = task;
    }
    public Pagetask(Page page, Task task, Set<Pagetaskinstance> pagetaskinstances, Set<Pagetaskinstitute> pagetaskinstitutes) {
       this.page = page;
       this.task = task;
       this.pagetaskinstances = pagetaskinstances;
       this.pagetaskinstitutes = pagetaskinstitutes;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="ID", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="PAGE", nullable=false)
    public Page getPage() {
        return this.page;
    }
    
    public void setPage(Page page) {
        this.page = page;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TASK", nullable=false)
    public Task getTask() {
        return this.task;
    }
    
    public void setTask(Task task) {
        this.task = task;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="pagetask")
    public Set<Pagetaskinstance> getPagetaskinstances() {
        return this.pagetaskinstances;
    }
    
    public void setPagetaskinstances(Set<Pagetaskinstance> pagetaskinstances) {
        this.pagetaskinstances = pagetaskinstances;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="pagetask")
    public Set<Pagetaskinstitute> getPagetaskinstitutes() {
        return this.pagetaskinstitutes;
    }
    
    public void setPagetaskinstitutes(Set<Pagetaskinstitute> pagetaskinstitutes) {
        this.pagetaskinstitutes = pagetaskinstitutes;
    }




}


