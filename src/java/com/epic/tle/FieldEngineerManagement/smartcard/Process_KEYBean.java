/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.FieldEngineerManagement.smartcard;

/**
 *
 * @author kreshan
 */

public class Process_KEYBean {
	private String MEK="";
	private String TMK="";
	private String FWK="";
	private String eFWK="";
	private String FWK_KVC="";
	private String eTMK="";
	private String TMK_KVC="";
	private String eMEK="";
	private String MEK_KVC="";
	private String RSA_MODULE="";
	private String RSA_EXPORNET="";
	private String PINBLOCK="";
	private String PIN="";
	private String KEYID="";
	
	public String getKEYID() {
		return KEYID;
	}
	public void setKEYID(String keyid) {
		KEYID = keyid;
	}
	public String getETMK() {
		return eTMK;
	}
	public void setETMK(String etmk) {
		eTMK = etmk;
	}
	public String getTMK_KVC() {
		return TMK_KVC;
	}
	public void setTMK_KVC(String tmk_kvc) {
		TMK_KVC = tmk_kvc;
	}
	public String getEMEK() {
		return eMEK;
	}
	public void setEMEK(String emek) {
		eMEK = emek;
	}
	public String getMEK_KVC() {
		return MEK_KVC;
	}
	public void setMEK_KVC(String mek_kvc) {
		MEK_KVC = mek_kvc;
	}
	public String getRSA_MODULE() {
		return RSA_MODULE;
	}
	public void setRSA_MODULE(String rsa_module) {
		RSA_MODULE = rsa_module;
	}
	public String getRSA_EXPORNET() {
		return RSA_EXPORNET;
	}
	public void setRSA_EXPORNET(String rsa_expornet) {
		RSA_EXPORNET = rsa_expornet;
	}
	public String getPINBLOCK() {
		return PINBLOCK;
	}
	public void setPINBLOCK(String pinblock) {
		PINBLOCK = pinblock;
	}
	public String getPIN() {
		return PIN;
	}
	public void setPIN(String pin) {
		PIN = pin;
	}
	public String getEFWK() {
		return eFWK;
	}
	public void setEFWK(String efwk) {
		eFWK = efwk;
	}
	public String getFWK_KVC() {
		return FWK_KVC;
	}
	public void setFWK_KVC(String fwk_kvc) {
		FWK_KVC = fwk_kvc;
	}
	public String getFWK() {
		return FWK;
	}
	public void setFWK(String fwk) {
		FWK = fwk;
	}
	public String getMEK() {
		return MEK;
	}
	public void setMEK(String mek) {
		MEK = mek;
	}
	public String getTMK() {
		return TMK;
	}
	public void setTMK(String tmk) {
		TMK = tmk;
	}
	

}
