/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.userManagement.service;

import com.epic.tle.mapping.EpicTleUser;
import com.epic.tle.userManagement.bean.RegisterUserBean;
import com.epic.tle.util.ExcelCommon;
import com.epic.tle.util.HibernateInit;
import com.epic.tle.util.Util;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author ridmi_g
 */
public class ExcelReportUsers {
    private static final int columnCount = 1;
    private static final int headerRowCount = 0;

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public static Object generateExcelReport(RegisterUserBean inputBean) throws Exception {
        Session session = null;
        Object returnObject = null;
        Query queryCount;
        Query querySearch = null;
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String directory = Util.getOSLogPath("/tmp/usersTemporary");

            File file = new File(directory);
            deleteDir(file);
            long count = 0;

            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            
            String orderBy = " order by wu.createdate desc ";
            if (inputBean.getSearchstatus() != null && !inputBean.getSearchstatus().equals("-1")) {
                String sqlCount = "select count(username) from EpicTleUser wu where wu.name LIKE :name and wu.epicTleUserProfile.description LIKE :userRole and wu.epicTleStatus.code LIKE :statuscode" + orderBy;
                queryCount = session.createQuery(sqlCount);
                queryCount.setParameter("name", "%" + inputBean.getSearchName() + "%");
                queryCount.setParameter("userRole", "%" + inputBean.getUserRole() + "%");
                queryCount.setString("statuscode", "%" + inputBean.getSearchstatus() + "%");
                Iterator itCount = queryCount.iterate();
                count = (Long) itCount.next();
            } else {
                String sqlCount = "select count(username) from EpicTleUser wu where wu.name LIKE :name and wu.epicTleUserProfile.description LIKE :userRole" + orderBy;
                queryCount = session.createQuery(sqlCount);
                queryCount.setParameter("name", "%" + inputBean.getSearchName() + "%");
                queryCount.setParameter("userRole", "%" + inputBean.getUserRole() + "%");
                Iterator itCount = queryCount.iterate();
                count = (Long) itCount.next();
            }

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
                if (count == 0) {
                    XSSFWorkbook workbook = new XSSFWorkbook();
                    XSSFSheet sheet = workbook.createSheet("Users Report");
                    sheet.autoSizeColumn((int) count);
                    ExcelReportUsers.createExcelTableHeaderSection(workbook, 0);
                    returnObject = workbook;
//                      returnObject = "empty";
                }
            }
            if (count > 0) {
                long maxRow = Long.parseLong("10000");
                int currRow = headerRowCount;
                int fileCount = 0;
                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet sheet = workbook.createSheet("Users Report");
                currRow = ExcelReportUsers.createExcelTableHeaderSection(workbook, currRow);
                int selectRow = 10000;
                int numberOfTimes = (int) (count / selectRow);
                if ((count % selectRow) > 0) {
                    numberOfTimes += 1;
                }
                int from = 0;
                int listrownumber = 1;

                if (inputBean.getSearchstatus() != null && !inputBean.getSearchstatus().equals("-1")) {
                    String sqlSearch = "from EpicTleUser wu where wu.name LIKE :name and wu.epicTleUserProfile.description LIKE :userRole and wu.epicTleStatus.code LIKE :statuscode" + orderBy;
                     querySearch = session.createQuery(sqlSearch);
                    querySearch.setParameter("name", "%" + inputBean.getSearchName() + "%");
                    querySearch.setParameter("userRole", "%" + inputBean.getUserRole() + "%");
                    querySearch.setString("statuscode", "%" + inputBean.getSearchstatus() + "%");
                    
                } else {
                    String sqlSearch = "from EpicTleUser wu where wu.name LIKE :name and wu.epicTleUserProfile.description LIKE :userRole" + orderBy;
                     querySearch = session.createQuery(sqlSearch);
                    querySearch.setParameter("name", "%" + inputBean.getSearchName() + "%");
                    querySearch.setParameter("userRole", "%" + inputBean.getUserRole() + "%");
                }

                Iterator itSearch = querySearch.iterate();
                querySearch.setFirstResult(from);
                querySearch.setMaxResults(selectRow);
                for (int i = 0; i < numberOfTimes; i++) {
                    while (itSearch.hasNext()) {
                        RegisterUserBean databean = new RegisterUserBean();
                    EpicTleUser objBean = (EpicTleUser) itSearch.next();

                        try {
                        databean.setName(objBean.getName());
                    } catch (NullPointerException npe) {
                        databean.setName("--");
                    }
                    try {
                        databean.setEmail(objBean.getEmail().isEmpty() ? "--" : objBean.getEmail());
                    } catch (NullPointerException npe) {
                        databean.setEmail("--");
                    }
                    try {
                        databean.setUserName(objBean.getUsername());
                    } catch (NullPointerException npe) {
                        databean.setUserName("--");
                    }
                    try {
                        databean.setUserType(objBean.getEpicTleUserProfile().getDescription());
                    } catch (NullPointerException npe) {
                        databean.setUserType("--");
                    }
                    try {
                        databean.setUserTypeId("" + objBean.getEpicTleUserProfile().getCode());
                    } catch (NullPointerException npe) {
                        databean.setUserType("--");
                    }
                    try {
                        databean.setStatusType("" + objBean.getEpicTleStatus().getCode());
                    } catch (NullPointerException npe) {
                        databean.setUserType("--");
                    }
                    try {
                        databean.setStstusDes(objBean.getEpicTleStatus().getDescription());
                    } catch (NullPointerException npe) {
                        databean.setStstusDes("--");
                    }
                    try {
                        databean.setDate(objBean.getCreatedate().toString());
                    } catch (NullPointerException npe) {
                        databean.setDate("--");
                    }
                    databean.setFullCount(count);
                        if (currRow + 1 > maxRow) {
                            fileCount++;
                            ExcelReportUsers.writeTemporaryFile(workbook, fileCount, directory);
                            workbook = ExcelReportUsers.createExcelTopSection(inputBean);
                            sheet = workbook.getSheetAt(0);
                            currRow = headerRowCount;
                            ExcelReportUsers.createExcelTableHeaderSection(workbook, currRow);
                        }
                        currRow = ExcelReportUsers.createExcelTableBodySection(workbook, databean, currRow, listrownumber);
                        listrownumber++;
                    }

                    from = from + selectRow;
                }

                if (fileCount > 0) {
                    fileCount++;
                    ExcelReportUsers.writeTemporaryFile(workbook, fileCount, directory);
                    ByteArrayOutputStream outputStream = ExcelCommon.zipFiles(file.listFiles());
                    returnObject = outputStream;
                } else {
                    for (int i = 0; i < columnCount; i++) {
                        //to auto size all column in the sheet
                        sheet.autoSizeColumn(i);
                    }
                    returnObject = workbook;
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.clear();
                session.getTransaction().commit();
                session.close();

            }
        }
        return returnObject;
    }

    private static XSSFWorkbook createExcelTopSection(RegisterUserBean inputBean) throws Exception {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Users Report");
        return workbook;
    }

    private static int createExcelTableHeaderSection(XSSFWorkbook workbook, int currrow) throws Exception {
        XSSFCellStyle columnHeaderCell = ExcelCommon.getColumnHeadeCell(workbook);
        XSSFSheet sheet = workbook.getSheetAt(0);
        Row row = sheet.createRow(currrow++);

        Cell cell = row.createCell(0);
        cell.setCellValue("Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(1);
        cell.setCellValue("User Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(2);
        cell.setCellValue("Email");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(3);
        cell.setCellValue("User Role");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(4);
        cell.setCellValue("Status");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(5);
        cell.setCellValue("Date");
        cell.setCellStyle(columnHeaderCell);


        return currrow;
    }

    private static void writeTemporaryFile(XSSFWorkbook workbook, int fileCount, String directory) throws Exception {
        File file;
        FileOutputStream outputStream = null;
        try {
            XSSFSheet sheet = workbook.getSheetAt(0);
            for (int i = 0; i < columnCount; i++) {
                //to auto size all column in the sheet
                sheet.autoSizeColumn(i);
            }

            file = new File(directory);
            if (!file.exists()) {
                System.out.println("Directory created or not : " + file.mkdirs());
            }
            System.out.println(file.getAbsolutePath());
            System.out.println(file.getCanonicalPath());
            if (fileCount > 0) {
                file = new File(directory + File.separator + "Users Report_" + fileCount + ".xlsx");
            } else {
                file = new File(directory + File.separator + "Users Report.xlsx");
            }
            outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
        } catch (IOException e) {
            throw e;
        } finally {
            if (outputStream != null) {
                outputStream.flush();
                outputStream.close();
            }
        }
    }

    private static int createExcelTableBodySection(XSSFWorkbook workbook, RegisterUserBean dataBean, int currrow, int rownumber) throws Exception {
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFCellStyle rowColumnCell = ExcelCommon.getRowColumnCell(workbook);
        Row row = sheet.createRow(currrow++);

        Cell cell = row.createCell(0);
        cell.setCellValue(dataBean.getName());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(1);
        cell.setCellValue(dataBean.getUserName());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(2);
        cell.setCellValue(dataBean.getEmail());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(3);
        cell.setCellValue(dataBean.getUserType());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(4);
        cell.setCellValue(dataBean.getStstusDes());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(5);
        cell.setCellValue(dataBean.getDate());
        cell.setCellStyle(rowColumnCell);

        return currrow;
    }
    
}
