/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.FieldEngineerManagement.service;

import com.epic.tle.FieldEngineerManagement.bean.MPOSKeyUtilityBean;
import com.epic.tle.util.KeyInfor;

/**
 *
 * @author ridmi_g
 */
public interface MPOSKeyUtilityInf {
   
    public MPOSKeyUtilityBean getPagePath(String page, MPOSKeyUtilityBean inputBean) throws Exception;
    
    public boolean isAlreadyExsitTID(String t_id) throws Exception;
    
    public boolean updateTerminal(KeyInfor infor,String tid) throws Exception;
    

}
