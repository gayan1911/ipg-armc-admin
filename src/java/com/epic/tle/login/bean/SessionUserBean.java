/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.login.bean;

import com.epic.tle.util.constant.Configurations;

/**
 *
 * @author kreshan
 */
public class SessionUserBean {
    private String name;
    private String userName;
    private String email;
    private String userRole;
    private int userLevel;
    private String currentSessionId;
    private int id;
    
    private String password;
    
    //tms/mer
    private String userrole_mer;
    private int userrole_tms;
    private int db_m_status;
    private int db_tms_status;

   
    public int getDb_m_status() {
        return db_m_status;
    }

    public void setDb_m_status(int db_m_status) {
        this.db_m_status = db_m_status;
    }

    public int getDb_tms_status() {
        return db_tms_status;
    }

    public void setDb_tms_status(int db_tms_status) {
        this.db_tms_status = db_tms_status;
    }

     
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserrole_mer() {
        return userrole_mer;
    }

    public void setUserrole_mer(String userrole_mer) {
        this.userrole_mer = userrole_mer;
    }

    public int getUserrole_tms() {
        return userrole_tms;
    }

    public void setUserrole_tms(int userrole_tms) {
        this.userrole_tms = userrole_tms;
    }
    
    
    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrentSessionId() {
        return currentSessionId;
    }

    public void setCurrentSessionId(String currentSessionId) {
        this.currentSessionId = currentSessionId;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Override
    public String toString() {
        
        if(Configurations.DB_M_STATUS == 1 || Configurations.DB_M_STATUS == 1){
            return "SessionUserBean{" + " userName=" + userName  + ", currentSessionId=" + currentSessionId + ", userroleMER=" + userrole_mer + ", userroleTMS=" + userrole_tms + ", password=" + password + '}';
        }else{
            return "SessionUserBean{" + "name=" + name + ", userName=" + userName + ", userRole=" + userRole + ", userLevel=" + userLevel + ", currentSessionId=" + currentSessionId + ", id=" + id + '}';
        }
        
//        return "SessionUserBean{" + "name=" + name + ", userName=" + userName + ", userRole=" + userRole + ", userLevel=" + userLevel + ", currentSessionId=" + currentSessionId + ", id=" + id + '}';
    }
   
}
