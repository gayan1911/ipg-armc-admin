/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.util;

import com.epic.tle.util.constant.Configurations;
import com.epic.tle.util.constant.DbConfiguraitonBean;
import com.epic.tle.util.constant.SystemMessage;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.w3c.dom.Document;

/**
 *
 * @author ridmi_g
 */
public class HibernateInit_Tms {
    public static SessionFactory sessionFactory_tms;
    
     {
        if (sessionFactory_tms == null || sessionFactory_tms.isClosed()) {
            initialize_tms();
        }
    }
     private void setDbConfig(Configuration config) {
        String path = Configurations.PATH_ROOT + Configurations.PATH_CONFIG;
        DbConfiguraitonBean dbConfigBean = new DbConfiguraitonBean();
        try {
            File fXmlFile = new File(path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            Configurations.TMS_URL = doc.getElementsByTagName("tmsurl").item(0).getTextContent();
            System.out.println(">>>>> tmsurl "+Configurations.TMS_URL);
            
            config.configure("hibernate_tms.cfg.xml");
            config.setProperty("hibernate.dialect", doc.getElementsByTagName("dbdialect").item(0).getTextContent());
            config.setProperty("hibernate.connection.driver_class",doc.getElementsByTagName("dbpooldriver").item(0).getTextContent());
            config.setProperty("hibernate.connection.datasource",doc.getElementsByTagName("tmsdbpoolsource").item(0).getTextContent());
            config.setProperty("hibernate.connection.zeroDateTimeBehavior","convertToNull");
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(SystemMessage.CONFIGURAITON_ERROR);
        }
            
            
//            config.setProperty("hibernate.connection.autocommit", "false");
             
    }
     public SessionFactory initialize_tms() {
        if (this.sessionFactory_tms == null || this.sessionFactory_tms.isClosed()) {
            Configuration configuration = new Configuration();
            setDbConfig(configuration);
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            this.sessionFactory_tms = configuration.buildSessionFactory(serviceRegistry);
        }
        return this.sessionFactory_tms;
    }
}
