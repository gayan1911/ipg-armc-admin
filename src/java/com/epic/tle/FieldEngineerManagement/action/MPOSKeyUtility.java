/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.FieldEngineerManagement.action;

import com.epic.tle.FieldEngineerManagement.bean.MPOSKeyUtilityBean;
import com.epic.tle.FieldEngineerManagement.service.MPOSKeyUtilityFactory;
import com.epic.tle.FieldEngineerManagement.smartcard.HSMConnector;
import com.epic.tle.login.bean.SessionUserBean;
import com.epic.tle.login.bean.TaskBean;
import com.epic.tle.util.AccessControlService;
import com.epic.tle.util.Common;
import com.epic.tle.util.IKGenerate;
import com.epic.tle.util.KeyInfor;
import com.epic.tle.util.LogFileCreator;
import com.epic.tle.util.Util;
import com.epic.tle.util.constant.LogTypes;
import com.epic.tle.util.constant.PageVarList;
import com.epic.tle.util.constant.SystemMessage;
import com.epic.tle.util.constant.SystemModule;
import com.epic.tle.util.constant.SystemSection;
import com.epic.tle.util.constant.TaskVarList;
import com.epic.tle.util.constant.TokenConst;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author ridmi_g
 */
public class MPOSKeyUtility extends ActionSupport implements AccessControlService, ModelDriven<MPOSKeyUtilityBean> {

    private MPOSKeyUtilityBean inputBean = new MPOSKeyUtilityBean();
    SessionUserBean session;
    MPOSKeyUtilityFactory service;
    

    public MPOSKeyUtilityFactory getService() {
        return new MPOSKeyUtilityFactory();
    }

    public SessionUserBean getSession() {
        return (SessionUserBean) ServletActionContext.getRequest().getSession(false).getAttribute("SessionObject");
    }

    public String getSessionToken() {
        return (String) ServletActionContext.getRequest().getSession().getAttribute(TokenConst.SESSION_TOKEN);
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    @Override
    public boolean checkAccess(String method, int userRole) {

        boolean status;
        applyUserPrivileges();
        String page = PageVarList.MPOS_KEY_UTILITY;
        inputBean.setPageCode(page);
        String task = null;
       if ("GenTID".equals(method)) {
            task = TaskVarList.VIEW;
        } else if ("downloadpdf".equals(method)) {
            task = TaskVarList.VIEW;
        } 
        if ("execute".equals(method)) {
            status = !inputBean.isView();
        } else {
            HttpSession session = ServletActionContext.getRequest().getSession(false);
            status = new Common().checkMethodAccess(task, page, session);
        }
        return status;

    }

    private boolean applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<TaskBean> tasklist = new Common().getUserTaskListByPage(PageVarList.MPOS_KEY_UTILITY, request);
     
        inputBean.setView(true);
        if (tasklist != null && tasklist.size() > 0) {
            for (TaskBean task : tasklist) {
                if (task.getTASK_ID().equalsIgnoreCase(TaskVarList.VIEW)) {
                    inputBean.setView(false);
                }
            }
        }
        return true;
    }
     @Override
    public MPOSKeyUtilityBean getModel() {
        try {
            getService().getKeyUtilityInf().getPagePath(inputBean.getPageCode(), inputBean);
        } catch (Exception e) {
        }
        return inputBean;
    }

   

    public String GenTID() throws Exception{
        inputBean.setToken(getSessionToken());
        try {
            inputBean.setMtid(inputBean.getTid());
         
                boolean ok = getService().getKeyUtilityInf().isAlreadyExsitTID(inputBean.getTid());
                if (ok == true) {

                    KeyInfor keyInfor = IKGenerate.getIK(HSMConnector.getPROVIDER(), inputBean.getTid());
                    if(keyInfor.isPROCESSIG()){
                        inputBean.setIk(keyInfor.getIK());
                        inputBean.setIk_kvc(keyInfor.getIK_KVC());
                        inputBean.setMackey(keyInfor.getMK());
                        inputBean.setMac_kvc(keyInfor.getMK_KVC());
                        inputBean.setIk_ksn(keyInfor.getIK_KSN());

                        if(getService().getKeyUtilityInf().updateTerminal(keyInfor, inputBean.getTid())){
                            
                            Util.insertHistoryRecord(LogTypes.TLEWEBAPP, getSession().getUserLevel(), 
                                    SystemModule.FIELD_ENGINEER_MANAGEMENT, TaskVarList.GENERATE, SystemMessage.FIELD_KEY_UTI_GEN_SUCCESS, null, null, null, 
                                    getSession().getId(), SystemSection.MPOS_KEY_UTILITY, null, null);
                            
                            LogFileCreator.writeInforToLog(SystemMessage.FIELD_KEY_UTI_GEN_SUCCESS);
                            inputBean.setMessage(SystemMessage.FIELD_KEY_UTI_GEN_SUCCESS);
                            inputBean.setSuccess(true);
                        }else{
                            inputBean.setMessage(SystemMessage.FIELD_KEY_UTI_GEN_FAIL);
                            inputBean.setSuccess(false);
                        }
                    }else{
                        inputBean.setMessage(SystemMessage.FIELD_KEY_UTI_GEN_FAIL);
                        inputBean.setSuccess(true);
                    }
                }else{
                    inputBean.setMessage(SystemMessage.FIELD_KEY_UTI_NOT_EX_TID);
                    inputBean.setSuccess(false);
                }
        } catch (Exception e) {
            e.printStackTrace();
            addActionError(SystemMessage.FIELD_KEY_UTI_GEN_FAIL);
            LogFileCreator.writeErrorToLog(e);
            
            throw e;
        }
        return "generate";
    }
    
    public String downloadpdf() {
        try {
                    inputBean.setFilename("Key_Details.pdf");
                    Map reportParameters = new HashMap();
                    reportParameters.put("tid", inputBean.getMtid());
                    reportParameters.put("ik", inputBean.getIk());
                    reportParameters.put("ik_kvc", inputBean.getIk_kvc());
                    reportParameters.put("mackey", inputBean.getMackey());
                    reportParameters.put("mac_kvc", inputBean.getMac_kvc());
                    reportParameters.put("ik_ksn", inputBean.getIk_ksn());
                    
                    inputBean.setParameterMap(reportParameters);
                    
                    List datalist = new ArrayList();
                    datalist.add(reportParameters);
                    inputBean.setReportdatalist(datalist);
                    return "pdfDownloadKey";
           
        } catch (Exception e) {
            e.printStackTrace();
            LogFileCreator.writeErrorToLog(e);
        }
        return SUCCESS;
    }
   

}
