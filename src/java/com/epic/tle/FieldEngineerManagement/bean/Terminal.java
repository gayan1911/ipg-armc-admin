/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.FieldEngineerManagement.bean;


/**
 *
 * @author KRESHAN
 */
public class Terminal {
    private String tid;
    private String mid;
    private String serialNo;
    private String bank;
    private String name;
    private String location;
    private String regDate;
    private int status;
    private int encStatus;
    private String encStatusName;
    private String statusName;
    private String brand;
    private String lsatTxnDate;
    private int nonEncTxnStatus;
    
    
    private String ETMKSESSIONKEY;
    private String EMKESESSIONKEY;
    
    private String TMKKVC;
    private String MEKKVC;
    private String KEYID;
    private String COUNTOR;
    
    private String MEK;
    private String TMK;
    
    private String Repbankaname;
    private String Repterminalid;
    private String Repmerchantname;
    private String Repmek;
    private String RepissuedDate;
    private String Repkeyid;
    private String Reptmk;

    public String getRepbankaname() {
        return Repbankaname;
    }

    public void setRepbankaname(String Repbankaname) {
        this.Repbankaname = Repbankaname;
    }

    public String getRepterminalid() {
        return Repterminalid;
    }

    public void setRepterminalid(String Repterminalid) {
        this.Repterminalid = Repterminalid;
    }

    public String getRepmerchantname() {
        return Repmerchantname;
    }

    public void setRepmerchantname(String Repmerchantname) {
        this.Repmerchantname = Repmerchantname;
    }

    public String getRepmek() {
        return Repmek;
    }

    public void setRepmek(String Repmek) {
        this.Repmek = Repmek;
    }

    public String getRepissuedDate() {
        return RepissuedDate;
    }

    public void setRepissuedDate(String RepissuedDate) {
        this.RepissuedDate = RepissuedDate;
    }

    public String getRepkeyid() {
        return Repkeyid;
    }

    public void setRepkeyid(String Repkeyid) {
        this.Repkeyid = Repkeyid;
    }

    public String getReptmk() {
        return Reptmk;
    }

    public void setReptmk(String Reptmk) {
        this.Reptmk = Reptmk;
    }

    
    public String getCOUNTOR() {
        return COUNTOR;
    }

    public void setCOUNTOR(String COUNTOR) {
        this.COUNTOR = COUNTOR;
    }

    public String getEMKESESSIONKEY() {
        return EMKESESSIONKEY;
    }

    public void setEMKESESSIONKEY(String EMKESESSIONKEY) {
        this.EMKESESSIONKEY = EMKESESSIONKEY;
    }

    public String getETMKSESSIONKEY() {
        return ETMKSESSIONKEY;
    }

    public void setETMKSESSIONKEY(String ETMKSESSIONKEY) {
        this.ETMKSESSIONKEY = ETMKSESSIONKEY;
    }

    public String getKEYID() {
        return KEYID;
    }

    public void setKEYID(String KEYID) {
        this.KEYID = KEYID;
    }

    public String getMEK() {
        return MEK;
    }

    public void setMEK(String MEK) {
        this.MEK = MEK;
    }

    public String getMEKKVC() {
        return MEKKVC;
    }

    public void setMEKKVC(String MEKKVC) {
        this.MEKKVC = MEKKVC;
    }

    public String getTMK() {
        return TMK;
    }

    public void setTMK(String TMK) {
        this.TMK = TMK;
    }

    public String getTMKKVC() {
        return TMKKVC;
    }

    public void setTMKKVC(String TMKKVC) {
        this.TMKKVC = TMKKVC;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getEncStatus() {
        return encStatus;
    }

    public void setEncStatus(int encStatus) {
        this.encStatus = encStatus;
    }

    public String getEncStatusName() {
        return encStatusName;
    }

    public void setEncStatusName(String encStatusName) {
        this.encStatusName = encStatusName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLsatTxnDate() {
        return lsatTxnDate;
    }

    public void setLsatTxnDate(String lsatTxnDate) {
        this.lsatTxnDate = lsatTxnDate;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNonEncTxnStatus() {
        return nonEncTxnStatus;
    }

    public void setNonEncTxnStatus(int nonEncTxnStatus) {
        this.nonEncTxnStatus = nonEncTxnStatus;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }
    
    
}
