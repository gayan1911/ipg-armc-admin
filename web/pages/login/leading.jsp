<%-- 
    Document   : leading
    Created on : Jul 4, 2019, 3:35:38 PM
    Author     : ridmi_g
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"  %>  
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <!--<meta charset="UTF-8">-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta http-equiv="Content-Security-Policy" content="style-src 'self' https://domaingoeshere.com/css/fonts.css 'unsafe-inline' 'unsafe-eval'">
        <!--<meta http-equiv="Content-Security-Policy" content="default-src 'self' *.bootstrapcdn.com">-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.css">
        <title>ANDROID HUB</title>
        
        

        <s:set id="m_status" var="m_status"><s:property value="db_m_status"/></s:set>
        <s:set id="tms_status" var="tms_status"><s:property value="db_tms_status"/></s:set>
        <s:set id="encodestring" var="encodestring"><s:property value="encodestring"/></s:set>
        <s:set id="m_url" var="m_url"><s:property value="m_url"/></s:set>
        <s:set id="tms_url" var="tms_url"><s:property value="tms_urlss"/></s:set>

        <s:set id="loginflag" var="loginflag"><s:property value="loginflag"/></s:set>
        <jsp:include page="../../Styles.jsp" />
        
        
        <script>
            var m_status = '${m_status}';
            var tms_status = '${tms_status}';
            var encodestring = '${encodestring}';
            var loginflag = '${loginflag}';

        </script>
        <style>

            @font-face {
                font-family: 'SourceSan';
                src: url(${pageContext.request.contextPath}/assets/fonts/OpenSans-Regular.ttf);
                /* IE9 Compat Modes */
            }

            @font-face {
                font-family: 'SourceSanBold';
                src: url(${pageContext.request.contextPath}/assets/fonts/OpenSans-Bold.ttf);
                /* IE9 Compat Modes */
            }

            html,
            body {
                padding: 0;
                margin: 0;
                width: 100%;
                height: 100%;
                font-family: 'SourceSan';
            }

            .container{
                margin-bottom: 32px;
            }

            .fixed-background {
                position: fixed;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                background: url(${pageContext.request.contextPath}/assets/img/back.png);
                background-size: cover;
                z-index: -1;
            }

            .logo {
                padding-top: 50px;
                padding-bottom: 50px;
            }

            .card-content {
                position: relative;
                background: #ffffff;
                padding: 35px 15px;
                margin: 15px auto;
                max-height: 320px;
                border: 2px solid #DEE5F0;
                box-sizing: border-box;
                border-radius: 16px;
                box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.25);
            }

            a {
                text-decoration: none !important;
            }

            .product-card {
                padding-left: 32px;
                padding-right: 32px;
            }

            a:hover .card-content {
                border: 2px solid #DB7F05;
            }

            a:hover .arrow-acc {
                border: 2px solid #3A4A61;
            }

            a:hover .arrow {
                background: url(${pageContext.request.contextPath}/assets/img/rightArrowHover.svg);
                background-repeat: no-repeat;
                background-size: contain;
            }

            .card-content img {
                width: 88%;
            }

            .arrow-acc {
                width: 48px;
                height: 48px;
                position: absolute;
                bottom: -9px;
                left: calc(50% - 20px);
                background: white;
                border-radius: 25px;
                border: 2px solid #D0D8E2;
            }

            .arrow {
                background: url(${pageContext.request.contextPath}/assets/img/rightArrow.svg);
                height: 24px;
                width: 12px;
                margin-left: 18px;
                margin-top: 12px;
                background-repeat: no-repeat;
                background-size: contain;
            }

            .card-content h3 {
                font-family: 'SourceSanBold';
                letter-spacing: -2px;
            }

            .card-content h3 span {
                font-family: 'SourceSan';
                letter-spacing: 0;
            }

            .footer {
                position: absolute;
                bottom: 5px;
                width: 100%;
                height: 20px;
                font-size: 12px;
                text-align: center;
            }

            /* @media only screen and (min-width: 992px) {
                .card-content img {
                    width: 265px;
                }
            }         */

            @media only screen and (min-width: 768px) and (max-width: 992px) {
                .card-content h3 {
                    font-size: 18px;
                    font-family: 'SourceSanBold';
                }

                .arrow-acc {
                    width: 40px;
                    height: 40px;
                    position: absolute;
                    bottom: -6px;
                    left: calc(50% - 20px);
                    background: white;
                    border-radius: 25px;
                    border: 2px solid #D0D8E2;
                }

                .arrow {
                    background: url(${pageContext.request.contextPath}/assets/img/rightArrow.svg);
                    height: 18px;
                    width: 9px;
                    margin-left: 15px;
                    margin-top: 11px;
                    background-repeat: no-repeat;
                    background-size: contain;
                }
            }

            @media only screen and (min-width: 480px) and (max-width: 768px) {
                .card-content img {
                    width: 95%;
                }

                .card-content h3 {
                    font-size: 16px;
                    font-family: 'SourceSanBold';
                }

                .product-card {
                    padding-left: 16px;
                    padding-right: 16px;
                }

                .arrow-acc {
                    width: 40px;
                    height: 40px;
                    position: absolute;
                    bottom: -5px;
                    left: calc(50% - 20px);
                    background: white;
                    border-radius: 25px;
                    border: 2px solid #D0D8E2;
                }

                .arrow {
                    background: url(${pageContext.request.contextPath}/assets/img/rightArrow.svg);
                    height: 18px;
                    width: 9px;
                    margin-left: 15px;
                    margin-top: 11px;
                    background-repeat: no-repeat;
                    background-size: contain;
                }
            }

            @media only screen and (max-width: 480px) {
                .footer{
                    position: relative;
                }
                .logo img {
                    width: 200px;
                }

                .card-content img {
                    width: 192px;
                }

                .card-content h3 {
                    font-size: 20px;
                    font-family: 'SourceSanBold';
                }

                .col {
                    width: 100%;
                }
            }
        </style>
        <script type="text/javascript">

            var Db_m_status = '${db_m_status}';
            var Db_tms_status = '${tms_status}';

            var url_tms = '${tms_url}';
            var url_mer = '${m_url}';

            var tletab;
            var mertab;
            var tmstab;


            $(document).ready(function () {
                if (Db_m_status !== "1") {
                    $('#div_mer').hide();
                }
                if (Db_tms_status !== "1") {
                    $('#div_tms').hide();
                }
                if (loginflag !== "1") {
                   
//                    localStorage.setItem('logout-event', 'login' + Math.random());
                    var mertab1 = window.open('', sessionStorage.getItem("mertabname"), '');
                    mertab1.close();
                    var tmstab1 = window.open('', sessionStorage.getItem("tmstabname"), '');
                    tmstab1.close();
                    window.close();//tle close

                    $.ajax({
                        url: '${pageContext.request.contextPath}/logOut.action',
                        data: {},
                        dataType: "json",
                        type: "POST",
                        success: function (data) {
                            window.location = "${pageContext.request.contextPath}/pages/login/login.jsp";
                        },
                        error: function (xhr, textStatus, errorThrown) {
                        }
                    });
                }
            });

            function formSumbit(url, name) {
                var url = url;
                var windowoption = 'fullscreen=yes,scrollbars=2';
                var form = document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("action", url);
                form.setAttribute("target", name);

                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = "param";
                input.value = encodestring;
                form.appendChild(input);
                document.body.appendChild(form);

                window.open('', name, windowoption);
                form.target = name;
                form.submit();
                document.body.removeChild(form);

            }

            function loadMerchantAdmin() {
//                var url = 'http://192.168.1.114:8085/EAPGMA/CheckUserLogin.blb?session:' + encodestring;
//                var url = url_mer + '?param=' + encodestring;
//                mertab = window.open(url, 'EAPGMA', 'fullscreen=yes,scrollbars=1', 'true');


                formSumbit(url_mer, 'EAPGMA');


                if (typeof (Storage) !== "undefined") {
                    var ranKey = Math.floor((Math.random() * 100000) + 1);
                    sessionStorage.setItem("merkey", ranKey);
                    sessionStorage.setItem("mertabname", 'EAPGMA');
                    $("#browserTabKeyMer").val(ranKey);
                } else {
                    console.log("Storage Un-Defined");
                }
            }

            function loadTLE() {

                $.ajax({
                    url: '${pageContext.request.contextPath}/loginSuccess',
                    data: {},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        tletab = window.open("${pageContext.request.contextPath}/pages/login/home.jsp", 'TLE', 'fullscreen=yes,scrollbars=2', 'true');
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        if (xhr.responseText.includes("csrfError.jsp")) {
                            window.location.replace("${pageContext.request.contextPath}/pages/csrfError.jsp");
                        } else {
                            window.location.href = "${pageContext.request.contextPath}/pages/error.jsp";
                        }
                    }
                });


                //create tab key
                if (typeof (Storage) !== "undefined") {
                    var ranKey = Math.floor((Math.random() * 100000) + 1);
                    sessionStorage.setItem("tlekey", ranKey);
                    $("#browserTabKeyTle").val(ranKey);
                } else {
                    // Sorry! No Web Storage support..
                    console.log("Storage Un-Defined");
                }

            }


            function loadTMS() {
//                var url = 'http://192.168.1.114:8085/TMS/CheckUserLogin.blb?session:' + encodestring;
//                var url = url_tms + '?param=' + encodestring;
//                tmstab = window.open(url, 'TMS', 'fullscreen=yes,scrollbars=3', 'true');


                formSumbit(url_tms, 'TMS');


                if (typeof (Storage) !== "undefined") {
                    var ranKey = Math.floor((Math.random() * 100000) + 1);
                    sessionStorage.setItem("tmskey", ranKey);
                    sessionStorage.setItem("tmstab", tmstab);
                    sessionStorage.setItem("tmstabname", 'TMS');
                    $("#browserTabKeyTms").val(ranKey);
                } else {
                    // Sorry! No Web Storage support..
                    console.log("Storage Un-Defined");
                }
            }


            function ValidateSession() {

                $.ajax({
                    url: '${pageContext.request.contextPath}/checkSession',
                    data: {},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                        if (data.returnStatus != 'landing') {
                            window.location.href = "${pageContext.request.contextPath}/pages/login/login.jsp";
                        } else {
                            //do nothing
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        if (xhr.responseText.includes("csrfError.jsp")) {
                            window.location.replace("${pageContext.request.contextPath}/pages/csrfError.jsp");
                        } else {
                            window.location.href = "${pageContext.request.contextPath}/pages/error.jsp";
                        }
                    }
                });
            }

            window.addEventListener('storage', function (event) {
                if (event.key == 'logout-event') {
                    window.location.href = "${pageContext.request.contextPath}/pages/login/login.jsp";
                }
            });
        function logoutAppsLea() {
            loginflag ='0';
            localStorage.setItem('logout-event', 'login' + Math.random());
            window.location = "${pageContext.request.contextPath}/pages/login/leading.jsp";
        
        }
        </script>


    </head>

    <body> 
        
        <div class="banner">
            <header>
                <div class="top-nav">
                    <div class="right-links">
                                <a onclick="logoutAppsLea()" href="javascript:void(0);" id="logout2"  class="lnk-logout" title="Log Out"></a>
                    </div>

                </div>
            </header>
        </div>
        
        
        <br/>
        <div class="fixed-background"></div>
        <div class="container">

            <div class="logo text-center">
                <img src="${pageContext.request.contextPath}/assets/img/logo_login_normal.svg" width="265px" alt="">
            </div>
            <div class="row content">
                <div class="product-card col col-xs-4 col-sm-4 col-md-4" id="div_mer">
                    <a href="">
                        <div class="card-content text-center">
                            <div>
                                <a onclick="loadMerchantAdmin()" href="javascript:void(0);"><img src="${pageContext.request.contextPath}/assets/img/merchant_portal.svg" alt=""></a>
                                    <s:hidden name="browserTabKeyMer" id="browserTabKeyMer"/>
                            </div>
                            <div>
                                <h3>Merchant <span>Portal</span></h3>
                            </div>
                        </div>
                        <div class="arrow-acc">
                            <div class="arrow"></div>
                        </div>
                    </a>
                </div>
                <div class="product-card col col-xs-4 col-sm-4 col-md-4" id="div_tms">
                    <a href="">
                        <div class="card-content text-center">
                            <div>
                                <a onclick="loadTMS()" href="javascript:void(0);"><img src="${pageContext.request.contextPath}/assets/img/terminal_management-01.svg" alt=""></a>
                                    <s:hidden name="browserTabKeyTms" id="browserTabKeyTms"/>
                            </div>
                            <div>
                                <h3>Terminal <span>Management</span></h3>
                            </div>
                        </div>
                        <div class="arrow-acc">
                            <div class="arrow"></div>
                        </div>
                    </a>
                </div>
                <div class="product-card col col-xs-4 col-sm-4 col-md-4">
                    <a href="">
                        <div class="card-content text-center">
                            <div>
                                <a onclick="loadTLE()" href="javascript:void(0);"><img src="${pageContext.request.contextPath}/assets/img/tle_management-01.svg" alt=""></a>
                                    <s:hidden name="browserTabKeyTle" id="browserTabKeyTle"/>
                            </div>
                            <div>
                                <h3>Security <span>Management</span></h3>
                            </div>
                        </div>
                        <div class="arrow-acc">
                            <div class="arrow"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="footer">
            Android Experience Hub v 1.0 Powered by Epic Lanka (Pvt.) Ltd.
        </div>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.cookie.js"></script>
        <!--<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/utility_manager.js"></script>-->
 
    </body>


</html>