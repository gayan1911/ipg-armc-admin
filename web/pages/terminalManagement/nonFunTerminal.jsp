<%-- 
    Document   : nonFunTerminal
    Created on : Jul 17, 2019, 2:42:30 PM
    Author     : ridmi_g
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>

        <s:set id="vview" var="vview"><s:property value="view" default="true"/></s:set>
        <s:set id="vdownload" var="vdownload"><s:property value="download" default="true"/></s:set>
            <script>

                var vdownload = '${vdownload}';

        </script>

       
        <jsp:include page="../../Styles.jsp" />

        <script>

            $.subscribe('onclicksearch', function (event, data) {
                var fromdate = $('#fromdate').val();
                $("#gridtable").jqGrid('setGridParam', {postData: {fromdate: fromdate}});
                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
                $('#divmsg1').hide();
            });
            function resetSearchForm() {
                $today = new Date();
                $dd = $today.getDate();
                $mm = $today.getMonth() + 1;

                $yyyy = $today.getFullYear();
                if ($dd < 10) {
                    $dd = '0' + $dd;
                }
                if ($mm < 10) {
                    $mm = '0' + $mm;
                }
                $today = $yyyy + '-' + $mm + '-' + $dd;
                $('#fromdate').val($today);
                $('#searchbut').click();
                $('#divmsg1').hide();
            }
            function resetForm() {}

        </script>
    </head>

    <body style="overflow:hidden">
        <section class="app-content">
            <jsp:include page="../../header.jsp" /> 
            <div class="content innerpage">
                <!--Body Content-->

                <!-- Breadcrumb begin -->
                <div class="breadcrumb">
                    <s:property value="Module"/> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <s:property value="Section"/> <i class="fa fa-angle-double-right" aria-hidden="true"></i><span id="task" class="active">Search Terminals</span>
                </div>
                <!-- End -->

                <h1 class="page-title"><s:property value="Section"/><a href="#" class="lnk-back hide-element do-nothing"><i class="fa fa-arrow-left" aria-hidden="true"></i> back</a></h1>

                <div class="content-section data-form" id="searchdiv">
                    <div class="msg-panel error-login-msg">
                        <s:actionerror/>
                        <s:actionmessage/>
                    </div>
                    <div class="content-data">

                        <s:form id="nonTersearchForm" action="XSLcreatnonTerm" name="nonTersearchForm" theme="simple" method="post">
                            <div class="d-row">
                                <label class="left-col form-label">From Date</label>
                                <div class="right-col form-field">
                                    <sj:datepicker id="fromdate" name="fromdate" readonly="true" value="today" changeYear="true" buttonImageOnly="true" displayFormat="yy-mm-dd" cssClass="txt-input width-20" maxDate="today"/>
                                </div>
                            </div>
                    
                            <div class="d-row cpanel">
                                <label class="left-col">&nbsp;</label>
                                <div class="right-col">
                                    <sj:a id="searchbut" button="true" onClickTopics="onclicksearch"
                                          cssClass="btn default-button" role="button" disabled="#vfind" aria-disabled="false" >
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                        Search</sj:a>
                                                                   
                                   

                                        <div class="btn-wrap lnk-match"><i class="fa fa-reply-all" aria-hidden="true">

                                            </i><s:submit id="exportXLSbutton" button="true" disabled="#vdownload" value="Export"
                                                  cssClass="btn default-button" role="button" aria-disabled="false" targets="divmsg1">

                                        </s:submit></div>

                                     <sj:a button="true" onclick="resetSearchForm()" cssClass="btn reset-button"><i class="fa fa-times" aria-hidden="true"></i> Reset</sj:a>

                                </div>
                            </div>
                            <input type="hidden" name="RequstToken" class="RequstToken" value='<%=session.getAttribute("SessionToken")%>'/>
                        </s:form>
                    </div>

                </div>

                

                


                

                <!-- Grid data begin -->
                <div class="content-section">
                    <div class="content-data">
                        <h2 class="section-title">All Non Functional Registered Terminals</h2>
                        <!-- Error and success message panel begin -->
                        <div class="msg-panel del-user-msg" >
                            <div><i class="fa fa-times" aria-hidden="true"></i><span id="divmsg"></span></div>
                        </div>
                        <!-- End -->

                        <div id="tablediv" class="custom-grid">


                            <s:url var="listurl" action="ListnonTerm"/>
                            <sjg:grid
                                id="gridtable"
                                caption="All Registerd Non Functional Terminals"
                                dataType="json"
                                href="%{listurl}"
                                pager="true"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="10"                                
                                autowidth="true"
                                rownumbers="true"
                                onCompleteTopics="completetopics"
                                rowTotal="false"
                                viewrecords="true"
                                >
                                <sjg:gridColumn name="sid" title="sId"   hidden="true" />
                                <sjg:gridColumn name="tid" index="tid" title="TID" align="left" width="15" sortable="false"  /> 
                                <sjg:gridColumn name="mid" index="mid" title="MID" align="left" width="35" sortable="false"/>                    
                                <sjg:gridColumn name="serialNo" index="serialNo" title="Serial No"  align="left" width="25"  sortable="false"/>
                                <sjg:gridColumn name="terminalBrand"  index="terminalBrand" title="Terminal Brand"  width="25" align="center" sortable="false"/>
                                <sjg:gridColumn name="name" title="Name" index="name" align="left" width="15" sortable="false" />
                                <sjg:gridColumn name="bank"  title="Bank"  width="25" align="center" sortable="false"/>
                                <sjg:gridColumn name="location" index="location" title="Location" align="left" width="15" sortable="false"  /> 
                                <sjg:gridColumn name="registerDate" index="registerDate" title="Register Date" align="left" width="35" sortable="false"/>                    
                                <sjg:gridColumn name="lastTransDate" index="lastTransDate" title="Last Transaction Date"  align="left" width="25"  sortable="false"/>
                            </sjg:grid> 
                        </div> 
                    </div>
                </div>
            </div>
            <!-- End -->




        


            <!--End of Body Content-->

            <jsp:include page="../../footer.jsp" />
        </div><!--End of Wrapper-->
    </section>
    <script type="text/javascript">
        $(document).ready(function () {
            //Back button event
            $('.lnk-back').on('click', function () {
                backToMain();
                $('#task').empty();
                $('.lnk-back').addClass('hide-element');
                var text = ' Search Terminals';
                $('#task').append(text);
                return false;
            });

            $(document).ready(function () {

                setTimeout(function () {
                    $(window).trigger('resize');
                }, 500);

            });

        });
    </script>
</body>
</html>
