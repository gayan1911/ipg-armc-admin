/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.FieldEngineerManagement.service;

import com.epic.tle.FieldEngineerManagement.bean.MPOSKeyUtilityBean;
import com.epic.tle.mapping.EpicTleSection;
import com.epic.tle.mapping.EpicTleTerminal;
import com.epic.tle.util.HibernateInit;
import com.epic.tle.util.KeyInfor;
import com.epic.tle.util.Util;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author ridmi_g 
 */
public class MPOSKeyUtilityService implements MPOSKeyUtilityInf {

    @Override
    public MPOSKeyUtilityBean getPagePath(String page, MPOSKeyUtilityBean inputBean) throws Exception {
        if (page != null && page != "") {

            String module = page.substring(0, 2);
            Session session = null;
            String pagePath = "";

            try {

                session = HibernateInit.sessionFactory.openSession();
                session.beginTransaction();
                EpicTleSection epicTleSection = (EpicTleSection) session.get(EpicTleSection.class, page);
                String mod = epicTleSection.getEpicTleModule().getDescription();
                String sect = epicTleSection.getSectionName();

                inputBean.setModule(mod);
                inputBean.setSection(sect);

            } catch (Exception e) {
                if (session != null) {
                    session.getTransaction().rollback();
                    session.close();
                    session = null;
                }
                throw e;
            } finally {
                if (session != null) {
                    session.getTransaction().commit();
                    session.close();
                    session = null;
                }
            }

        }

        return inputBean;

    }
    @Override
    public boolean isAlreadyExsitTID(String t_id) throws Exception {
        boolean isTID = false;
        List<EpicTleTerminal> tleTerminals = null;
        Session session = null;
        Query query = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from EpicTleTerminal wu where wu.tid =:tid";
            query = session.createQuery(sql);
            query.setString("tid", t_id);
            tleTerminals = query.list();
            if (0 < tleTerminals.size()) {
                int statusCode = 0;
                for (int i = 0; i < tleTerminals.size(); i++) {
                    statusCode = tleTerminals.get(i).getEpicTleStatusByStatus().getCode();
                    if (statusCode == 1) {
                        isTID = true;
                    }
                }

            }
        } catch (Exception e) {
            if (session != null) {
                session.close();
                session = null;
            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
                session = null;
            }
        }
        return isTID;
    }

    @Override
    public boolean updateTerminal(KeyInfor infor,String tid) throws Exception {
        boolean isUpdated = false;
        Session session = null;
        Query query = null;
        EpicTleTerminal terminal = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            String sql = "from EpicTleTerminal wu where wu.tid =:ID";
            query = session.createQuery(sql);
            query.setString("ID", tid);
            terminal = (EpicTleTerminal) query.uniqueResult();
            if (terminal != null) {
                terminal.setKeyid(infor.getKID());
                terminal.setEtmksessionkey(infor.getENCR_IK());
                terminal.setEmkesessionkey(infor.getENCR_MK());
                terminal.setTmkkvc(infor.getIK_KVC());
                terminal.setMekkvc(infor.getMK_KVC());

                terminal.setBdkId(0);
                terminal.setLastupdatedate(Util.getLocalDate());
                
                session.update(terminal);
                session.getTransaction().commit();
                isUpdated = true;
            }
        } catch (Exception e) {
            if (session != null) {
                session.getTransaction().rollback();
                session.flush();
                session.close();
                session = null;
            }
            throw e;
        } finally {
            try {
                if (session != null) {
                    session.flush();
                    session.close();
                    session = null;
                }
            } catch (Exception e) {
                throw e;
            }
        }
        return isUpdated;
    }


}
