/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.util.alerts;

/**
 *
 * @author ridmi_g
 */

import com.epic.tle.util.constant.Configurations;
import com.sun.mail.util.MailSSLSocketFactory;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SMBAlertSender {

    public static boolean sendMail(String senderEmail, String reciverEmail, String subject, String message, String URL, String username, String password, int port) throws Exception {
        System.out.println("email sending start..!");
        Transport transport = null;
        boolean rc = false;
        try {

            String messageText = message;

            Properties props = System.getProperties();
            props.put("mail.host", URL);
            props.put("mail.transport.protocol.", "smtp");
            props.put("mail.smtp.auth", "false");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.timeout", Configurations.MAIL_TIMEOUT);
            props.put("mail.smtp.connectiontimeout", Configurations.MAIL_TIMEOUT);

            MailSSLSocketFactory sf = new MailSSLSocketFactory();

            sf.setTrustAllHosts(true);

            props.put("mail.smtp.ssl.socketFactory", sf);

            Session mailSession = Session.getDefaultInstance(props, null);

            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(senderEmail));
            InternetAddress[] address = {new InternetAddress(reciverEmail)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject);

            MimeMultipart multipart = new MimeMultipart("related");

            BodyPart messageBodyPart = new MimeBodyPart();
            String htmlText = messageText;
            messageBodyPart.setContent(htmlText, "text/html");

            multipart.addBodyPart(messageBodyPart);

            msg.setContent(multipart);

            transport = mailSession.getTransport("smtp");

            transport.connect();

            transport.sendMessage(msg, msg.getAllRecipients());

            rc = true;

        } catch (Exception e) {
            throw e;
        } finally {
            transport.close();
            transport = null;
        }
        return rc;
    }

//    public static boolean sendSMS(String sentNumber, String message, String URL, String password, String username, int timeout) throws Exception {
//        boolean ok = false;
//        String response = null;
//        URL url = null;
//        HttpURLConnection urlConnection = null;
//        BufferedWriter bWriter = null;
//        BufferedReader bReader = null;
//
//        try {
//
//            String parm[] = username.split("~");
//
//            String request = "{\"reqRef\" : \"" + TxnUtil.getSessionID().substring(0, 16) + "\",\"appcode\" : \"" + parm[0] + "\",\"servicecode\" : \"" + parm[1] + "\",\"password\" : \"" + password + "\",\"priority\" : \"1\",\"message\" : \"" + message + "\",\"mobile\" : \"" + sentNumber + "\"}";
//
//            TxnUtil.debug(null, SystemConfg.LOG_TYPE_ALERT, null, "SMS request message : " + request);
//
//            url = new URL((URL));
//
//            urlConnection = (HttpURLConnection) url.openConnection();
//            urlConnection.setRequestMethod("POST");
//            urlConnection.setDoOutput(true);
//            urlConnection.setRequestProperty("Content-Type", "application/json");
//            urlConnection.setRequestProperty("Accept", "application/json");
//            urlConnection.setConnectTimeout(timeout);
//
//            bWriter = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
//            bWriter.write(request);
//            bWriter.flush();
//
//            bReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//            String line;
//            while (null != ((line = bReader.readLine()))) {
//                response += line;
//            }
//            TxnUtil.debug(null, SystemConfg.LOG_TYPE_ALERT, null, "SMS delivered response message : " + response);
//            ok = true;
//
//        } catch (Exception e) {
//
//            throw e;
//        } finally {
//            try {
//                if (bWriter != null) {
//                    bWriter.close();
//                }
//                if (bReader != null) {
//                    bReader.close();
//                }
//                if (urlConnection != null) {
//                    urlConnection.disconnect();
//                }
//
//                urlConnection = null;
//                bWriter = null;
//                bReader = null;
//            } catch (Exception e) {
//
//            }
//        }
//
//        return ok;
//
//    }
}
