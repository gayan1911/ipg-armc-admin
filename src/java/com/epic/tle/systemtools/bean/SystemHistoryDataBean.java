/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.tle.systemtools.bean;

/**
 *
 * @author dimuthu_h
 */
public class SystemHistoryDataBean {
    
    private String sid;
    private String node;
    private String dateTime;
    private String logType;
    private String module;
    private String userType;
    private String webUser;
    private String location;
    private String operation;
    private String comment;
    private String serialNo;
    private String tid;
    private String mid;
    private long fullCount;
    private String section;
    
    private String newValue;
    private String oldValue;
    
    //REPORT
    private String NODE;
    private String USER;
    private String LOCATION;
    private String MODULE;
    private String SECTION;
    private String TASK;
    private String COMMENT;
    private String DATETIME;
    private String OLDVALUE;
    private String NEWVALUE;
    

    public String getNODE() {
        return NODE;
    }

    public void setNODE(String NODE) {
        this.NODE = NODE;
    }

    public String getUSER() {
        return USER;
    }

    public void setUSER(String USER) {
        this.USER = USER;
    }

    public String getLOCATION() {
        return LOCATION;
    }

    public void setLOCATION(String LOCATION) {
        this.LOCATION = LOCATION;
    }

    public String getMODULE() {
        return MODULE;
    }

    public void setMODULE(String MODULE) {
        this.MODULE = MODULE;
    }

    public String getSECTION() {
        return SECTION;
    }

    public void setSECTION(String SECTION) {
        this.SECTION = SECTION;
    }

    public String getTASK() {
        return TASK;
    }

    public void setTASK(String TASK) {
        this.TASK = TASK;
    }

    public String getCOMMENT() {
        return COMMENT;
    }

    public void setCOMMENT(String COMMENT) {
        this.COMMENT = COMMENT;
    }

    public String getDATETIME() {
        return DATETIME;
    }

    public void setDATETIME(String DATETIME) {
        this.DATETIME = DATETIME;
    }

    public String getOLDVALUE() {
        return OLDVALUE;
    }

    public void setOLDVALUE(String OLDVALUE) {
        this.OLDVALUE = OLDVALUE;
    }

    public String getNEWVALUE() {
        return NEWVALUE;
    }

    public void setNEWVALUE(String NEWVALUE) {
        this.NEWVALUE = NEWVALUE;
    }

    
    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getWebUser() {
        return webUser;
    }

    public void setWebUser(String webUser) {
        this.webUser = webUser;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }

    /**
     * @return the dateTime
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * @return the node
     */
    public String getNode() {
        return node;
    }

    /**
     * @param node the node to set
     */
    public void setNode(String node) {
        this.node = node;
    }

    
    public String getSection() {
        return section;
    }

    /**
     * @param section 
     */
    public void setSection(String section) {
        this.section = section;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }
 
}
